﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainManager : MonoBehaviour {
    [System.Serializable]
    public class Control {
        public Button playBtn;
        public Button eatBtn;
        public Button showerBtn;
        public Button miniGamesBtn;
    }
    [SerializeField]
    private PersManager pers;
    [SerializeField]
    private Transform persPanel;
    [SerializeField]
    private Control control;

    [SerializeField] private GameObject setNamePanel;
    [SerializeField] private Text nameLabel;
    [SerializeField] private Text ageLabel;
    [SerializeField] private Text levelLabel;
    [SerializeField] private Text healthLabel;
    [SerializeField] private Button openStatisticPanelBtn;
    [SerializeField] private GameObject chickenLeg;
    [SerializeField] private GameObject water;
    [SerializeField] private GameObject hand;
    
    public static MainManager instance;
    private List<DB_Tables.TasksTable> tasks;
    public static bool GameOnPause;
    private void Awake() {
        instance = this;
        GameOnPause = false;
        control.eatBtn.onClick.AddListener(ClickEatBtnListener);
        control.playBtn.onClick.AddListener(ClickPlayBtnListener);
        control.showerBtn.onClick.AddListener(ClickShowerBtnListener);
        control.miniGamesBtn.onClick.AddListener(ClickMiniGamesListener);
        openStatisticPanelBtn.onClick.AddListener(OpenStatisticPaneClicklListener);
        openStatisticPanelBtn.onClick.AddListener(OpenStatisticPaneClicklListener);
    }

    private void Start() {
        if (pers.GetPersValues().name == null ) {
            setNamePanel.SetActive(true);
        }
        else {
            setNamePanel.SetActive(false);
            SetNameLabel(pers.GetPersValues().name);
            this.pers.AddAge();
        }
        List<DB_Tables.Skins> allSkins = DB_Query.instance.GetAllSkinsByDressed(1);
        
        //Если нет сохраненого прогресса
        //И в игру зашли в превый раз то назначаем скин кота
        if (allSkins.Count == 0) {
            DB_Query.instance.DressedSkin(1,1);
            //ChangePers(allSkins.First().name);
        }else {
            ChangePers(allSkins.First().name);
        }
        
        LocalNotification.CancelAllNotifications();
        InvokeRepeating("UpdateShem",0,30);

        SetAgeLabel(pers.GetPersValues().age.ToString());
        SetLevelLabel(pers.GetPersValues().level.ToString());

        //Health ****************
        List<DB_Tables.TasksTable> missedList = DB_Query.instance.SelectTaskByStatus(Tasks.TskStatus.MISSED);
        if (missedList.Count > 0) {
            foreach (DB_Tables.TasksTable missed in missedList) {
                pers.TakeAwayHealth(50);
                DB_Query.instance.SetSatus(Tasks.TskStatus.TOOK_AWAY_HEALTH,DateTime.Now,missed.id);
            }
        } else {
            if (pers.GetPersValues().health < 100) {
                List<DB_Tables.TasksTable> tookAwayHealthList = DB_Query.instance.SelectTaskTookAwayHealth();
                if (tookAwayHealthList.Count > 0 && (DateTime.Now - tookAwayHealthList[0].time).Days >= 10) {
                    pers.AddHealth(50);
                    DB_Query.instance.UpdateTimeLead(DateTime.Now, tookAwayHealthList[0].id);
                }
            }
        }
        pers.UpdateValues();
        SetHealthLabel(pers.GetPersValues().health.ToString());
        SenderDataManager.instance.SendData();
    }

    void OnApplicationPause(bool pauseStatus) {
        if (!pauseStatus){
            LocalNotification.CancelAllNotifications();
            if(!IsInvoking("UpdateShem"))
                InvokeRepeating("UpdateShem", 0, 30);
            AvailableTasks();
        }
        else {
            //GetPersManager().gameObject.GetComponent<Image>().color = Color.blue;
            //if (IsInvoking("UpdateShem"))
            //CancelInvoke("UpdateShem");
            Schema.instance.LoadXmlSchem();
        }
    }

    private void Update() {
        if (timer > 0) {
            timer -= 1f*Time.deltaTime;
            return;
        }
        if (GameOnPause) {
            GameOnPause = false;
            LocalNotification.CancelAllNotifications();
        }

        if (Input.GetKeyUp(KeyCode.Escape)) {
            MessageManager.instance.StartConfirmWindow("Уже уходишь?",ExitGame);
        }

#if UNITY_EDITOR || UNITY_STANDALONE
        if (Input.GetKey(KeyCode.Z) && Input.GetKey(KeyCode.X) && Input.GetKey(KeyCode.C) &&
            Input.GetKeyDown(KeyCode.UpArrow)) {
            MessageManager.instance.StartConfirmWindow("Перейти в debug scene?",OpenDebugScene);
        }
#endif
    }

    private void OpenDebugScene() {
        SceneManager.LoadScene(6);
    }

    //Вызывается в Start методом invoke
    private void UpdateShem() {
        Schema.instance.UplloadSchem();
        Schema.instance.LoadXmlSchem();
        AvailableTasks();
    }

    public void ChangePers(string name) {
        if (pers != null) {
            Destroy(pers.gameObject);
        }
        Vector3 pos = new Vector3();
        switch (name) {
            case "Кот":
                pos = new Vector3(0f,-55f,0f);
                break;
            case "Собака":
                pos = new Vector3(-20f, -40f, 0f);
                break;
            case "Барс":
                pos = new Vector3(14f, -45f, 0f);
                break;
            default:
                pos = new Vector3(0f, -55f, 0f);
                break;
        }
        GameObject obj = Helper.InstantiateUiObject(
            pos,
            Resources.Load<GameObject>("Skins/characters/" + name),
            persPanel);
        pers = obj.GetComponent<PersManager>();
    }

    public void AvailableTasks() {
        control.eatBtn.interactable = false;
        control.playBtn.interactable = false;
        control.showerBtn.interactable = false;
        tasks = DB_Query.instance.SelectTaskByStatus(Tasks.TskStatus.WAIT);
        foreach (DB_Tables.TasksTable task in tasks) {
            int activeTime = Helper.GetTimeActiveTask(task.repeat_time);
            if (DateTime.Now >= task.time && DateTime.Now < task.time.AddHours(activeTime)) {
                switch (task.task) {
                    case Tasks.TasksType.EAT:
                        control.eatBtn.interactable = true;
                        break;
                    case Tasks.TasksType.PLAY:
                        control.playBtn.interactable = true;
                        break;
                    case Tasks.TasksType.WASH:
                        control.showerBtn.interactable = true;
                        break;
                    default:
                        break;
                }
            }
        }
    }

    public void ComplateTask(Tasks.TasksType taskName) {
        if(DB_Query.instance == null) return;
        List<DB_Tables.TasksTable> tasks = DB_Query.instance.SelectTaskByName(taskName,Tasks.TskStatus.WAIT);
        foreach (DB_Tables.TasksTable task in tasks) {
            if (DateTime.Now >= task.time && DateTime.Now < task.time.AddHours(Helper.GetTimeActiveTask(task.repeat_time))) {
                DB_Query.instance.SetComplateTask(DateTime.Now, task.id);

                switch (taskName) {
                    case Tasks.TasksType.EAT:
                        DB_Query.instance.SetEat(pers.GetPersValues().ate + 1);
                        break;
                    case Tasks.TasksType.PLAY:
                        DB_Query.instance.SetPlayed(pers.GetPersValues().played + 1);
                        break;
                        default:
                        break;
                }
            }
        }
        AvailableTasks();
    }

    public void SetNameLabel(string name) {
        nameLabel.text = name;
        CoinsManager.instance.UpdateCoins();
    }
    public void SetAgeLabel(string age) {
        ageLabel.text = "Age: " + age;
    }
    public void SetLevelLabel(string level) {
        levelLabel.text = "Level: " + level;
    }
    public void SetHealthLabel(string health) {
        healthLabel.text = "Health: " + health;
    }
    public void NewLevel() {
        if(DB_Query.instance == null) return;
        int level = this.pers.GetPersValues().level;
        float newLevel = GetLevelProgress();
        if (level <= newLevel) {
            DB_Query.instance.SetLevel((int)newLevel+1);
            this.pers.UpdateValues();
            MessageManager.instance.StartWaitWindow(
                string.Format("{0} {1} {2} {3}", 
                "Поздравляю!",
                Environment.NewLine,
                "Ты молодец, переходишь на уровень",
                pers.GetPersValues().level),
                NewSckin);
            SetLevelLabel(newLevel.ToString());

            AudioManager.instance.PlaySound(
                AudioSourcesEnums.BUTTONS,
                SoundEnums.LEVELUP
                );
        }
    }

    public void NewSckin() {
        Debug.Log("New Sckin!");
       /* if (this.pers.GetPersValues().level*10%20 == 0) {
            MessageManager.instance.StartWaitWindow("New scin!");
        }*/
    }

    private void ClickShowerBtnListener() {
        pers.Shower();
        Helper.InstantiateUiObject(new Vector3(114f, -210f, 0f), water, pers.transform.parent);
        NewLevel();
    }

    private void ClickPlayBtnListener() {
        pers.Play();
        Helper.InstantiateUiObject(new Vector3(75f, -195f, 0f), hand, pers.transform.parent);
        NewLevel();
    }

    private void ClickEatBtnListener() {
        pers.Eat();
        Helper.InstantiateUiObject(new Vector3(-75f, -195f, 0f), chickenLeg, pers.transform.parent);
        NewLevel();
    }
    //Открыть панель статистики
    private void OpenStatisticPaneClicklListener() {
        StatisticPanelManager.instance.ActivatePanel(
            pers.GetPersValues().name,
            pers.GetPersValues().age.ToString(),
            pers.GetPersValues().health.ToString(),
            pers.GetPersValues().ate.ToString(),
            /*DB_Query.instance.SelectTaskByName(Tasks.TasksType.WASH, Tasks.TskStatus.COMPLATE).Count.ToString(),*/
            pers.GetPersValues().played.ToString(),
            pers.GetPersValues().level.ToString());
    }

    private void ClickMiniGamesListener(){

    }

    public PersManager GetPersManager() {
        return pers;
    }

    public float GetLevelProgress() {
        int complateCount = DB_Query.instance.SelectTaskByStatus(Tasks.TskStatus.COMPLATE).Count;
        float f = complateCount % 10;
        if (f == 0) {
            return complateCount/10f;
        }
        return f / 10f;
    }

    public void KillKitten() {
        GetPersManager().TakeAwayHealth(GetPersManager().GetPersValues().health);
    }

    public void ResurrectKitten() {
        GetPersManager().AddHealth(100);
    }

    public void Complatetask() {
        DB_Query.instance.SetTask(Tasks.TasksType.DEFAULT, "Дебажная таска", "Дебажная таска", Tasks.TskStatus.COMPLATE, DateTime.Now, 1,24);
        NewLevel();
    }

    private float timer = 0;
    public void SetTimer() {
        timer = 1f;
    }

    private void OnDestroy() {
        SetTimer();
        Schema.instance.LoadXmlSchem();
    }

    private void ExitGame() {
        StartCoroutine(ExitGameIEnumerator());
    }

    private IEnumerator ExitGameIEnumerator() {
        yield return new WaitForEndOfFrame();
        Schema.instance.LoadXmlSchem();
        yield return new WaitForEndOfFrame();
        ProgressManager.instance.SaveProgress();
        yield return new WaitForEndOfFrame();
        Application.Quit();
    }
}
