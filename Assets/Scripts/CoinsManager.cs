﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinsManager : MonoBehaviour {
    public static CoinsManager instance;

    private void Awake() {
        instance = this;
    }

    public int GetCoins() {
        int coins = 0;
        int.TryParse(DB_Query.instance.GetCoins(), out coins);
        return coins;
    }

    public void AddCoins(int value) {
        DB_Query.instance.AddCoin(value,UpdateCoins);
    }

    public void TakeAwayCoin(int value) {
        DB_Query.instance.TakeAwayCoin(value, UpdateCoins);
    }

    public void UpdateCoins() {
        StoreManager.instance.GetCoinsLabel().text = GetCoins().ToString();
    }
}
