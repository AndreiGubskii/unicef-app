﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class MiniGamesPanelManager : MonoBehaviour {
    [SerializeField] private Button duckHuntBtn;
    [SerializeField] private Button spaceShooterBtn;
    [SerializeField] private Button jumperBtn;

    [SerializeField] private Button openGamesPanel;
    [SerializeField] private Button clouseGamesPanel;

    [SerializeField] private GameObject panel;
    private void Awake() {
        duckHuntBtn.onClick.AddListener(DuckHuntClickListener);
        spaceShooterBtn.onClick.AddListener(SpaceShooterClickListener);
        jumperBtn.onClick.AddListener(JumperClickListener);
        openGamesPanel.onClick.AddListener(OpenPanelClickListener);
        clouseGamesPanel.onClick.AddListener(ClousePanelClickListener);
    }

    private void JumperClickListener() {
        LoadingManager.instance.LoadScene(5);
    }

    private void SpaceShooterClickListener() {
        LoadingManager.instance.LoadScene(4);
    }

    private void DuckHuntClickListener() {
        LoadingManager.instance.LoadScene(3);
    }

    private void ClousePanelClickListener() {
        panel.SetActive(false);
    }

    private void OpenPanelClickListener() {
        panel.SetActive(true);
    }


}
