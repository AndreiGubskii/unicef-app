﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandController : MonoBehaviour {

 private Mover _mover;
    private Animator _animator;
    [SerializeField] private float animationSpeed;

    private void Awake() {
        _mover = GetComponent<Mover>();
        _animator = GetComponent<Animator>();
    }

    private void Start() {
        _mover.InPlaceEvent += InPlaceListener;
        _animator.speed = animationSpeed;

        _mover.MoveTo(GameObject.Find("had").GetComponent<RectTransform>().anchoredPosition, 500);
    }

    private void InPlaceListener() {
        StartCoroutine(HandIenumerator());
    }

    private IEnumerator HandIenumerator() {
        _animator.SetTrigger("play");
        yield return new WaitForSeconds(3f);
        _animator.SetTrigger("stop");
        yield return new WaitForSeconds(0.3f);
        _mover.MoveTo(new Vector3(1000,this.transform.localPosition.y,0f), 500f);
        Destroy(gameObject, 5f);
    }
}
