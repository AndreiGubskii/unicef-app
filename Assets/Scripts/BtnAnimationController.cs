﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BtnAnimationController : MonoBehaviour {

    private Animator _animator;
    private Button btn;
    private bool animationPlayed;
    private void Awake() {
        _animator = GetComponent<Animator>();
        btn = GetComponent<Button>();
    }

    public void PlayAnimation() {
        _animator.SetTrigger("play");
        animationPlayed = true;
    }

    public void StopAnimation() {
        _animator.SetTrigger("stop");
        animationPlayed = false;
    }

    private void Update() {
        if (btn.interactable && !animationPlayed) {
            PlayAnimation();
        }
        else if(!btn.interactable && animationPlayed) {
            StopAnimation();
        }
    }
}
