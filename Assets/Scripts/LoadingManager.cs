﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LoadingManager : MonoBehaviour {

    public static LoadingManager instance;
    private AsyncOperation acync;

    [SerializeField] private Slider slider;
    [SerializeField] private Image back;
    [SerializeField] private Transform panel;
    private void Awake() {
        instance = this;
    }

    public void LoadScene(int scene) {
        StartCoroutine(LoadSceneIEnumerator(scene));
    }

    private IEnumerator LoadSceneIEnumerator(int scene) {
        LoadUiElements(scene);
        acync = SceneManager.LoadSceneAsync(scene);
        acync.allowSceneActivation = false;

        while (!acync.isDone) {
            slider.value = acync.progress;

            if (acync.progress == 0.9f) {
                slider.value = 1f;
                acync.allowSceneActivation = true;
            }
            yield return null;
        }
    }

    private void LoadUiElements(int scene) {
        panel.gameObject.SetActive(true);
        back.sprite = Resources.Load<Sprite>("Arts/loadingUi/backs/"+scene);
    }
}
