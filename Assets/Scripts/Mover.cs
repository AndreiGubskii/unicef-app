﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mover : MonoBehaviour {
    public delegate void MoverDelegate();

    public event MoverDelegate InPlaceEvent;
    [SerializeField] private bool moveOnStart;
    [SerializeField] private float speed;
    [SerializeField] private Vector3 target;
    private bool move;

    private void Start() {
        move = moveOnStart;
    }

    private void Update() {
        if (move) {
            if (Vector3.Distance(this.transform.localPosition, target) > 1 ) {
                this.transform.localPosition = Vector3.MoveTowards(this.transform.localPosition,target,speed*Time.deltaTime);
            }
            else {
                if (InPlaceEvent != null) {
                    InPlaceEvent();
                }
                move = false;
            }
        }
    }

    public void MoveTo(Vector3 _target,float speed) {
        this.speed = speed;
        target = _target;
        move = true;
    }

    public void Move() {
        MoveTo(this.target,this.speed);
    }
}
