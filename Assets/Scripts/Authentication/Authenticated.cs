﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Authenticated : MonoBehaviour {
    public delegate void AuthenticationEvents(object obj);

    public event AuthenticationEvents UserAuthenticationEvent;
    public event AuthenticationEvents UserNotAuthenticationEvent;
    public event AuthenticationEvents ErrorAuthenticationEvent;

    [Serializable]
    public class AuthenticatedInfo{
        public string is_authenticated;
        public string detail;
    }

    IEnumerator AuthenticatedIenumerator(string token){
        UnityWebRequest www = UnityWebRequest.Get("http://165.227.247.66/is_authenticated/");
        www.SetRequestHeader("Authorization", "Token " + token);
        yield return www.Send();

        if (www.isNetworkError || www.isHttpError){
            if (ErrorAuthenticationEvent != null){
                ErrorAuthenticationEvent(www.error);
            }
        }
        else {
            bool is_authenticated;
            bool.TryParse(JsonUtility.FromJson<AuthenticatedInfo>(www.downloadHandler.text).is_authenticated,out is_authenticated);
            if (is_authenticated){
                if (UserAuthenticationEvent != null){
                    UserAuthenticationEvent(is_authenticated);
                }
            }
            else {
                if (UserNotAuthenticationEvent != null){
                    UserNotAuthenticationEvent(is_authenticated);
                }
            }

        }
    }


    public void StartAuthenticated(string token){
        StartCoroutine(AuthenticatedIenumerator(token));
    }
}
