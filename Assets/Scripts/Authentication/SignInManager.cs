﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SignInManager : MonoBehaviour {
    [SerializeField] private Authorization authorization;
    [SerializeField] private Authenticated authenticated;

    [SerializeField] private InputField login;
    [SerializeField] private InputField password;
    [SerializeField] private Button submit;

    [SerializeField] private GameObject authorizationPanel;

    private const int  MAINSCENE = 2;
    private void Awake() {
        authenticated.UserAuthenticationEvent += UserAuthenticationListener;
        authenticated.UserNotAuthenticationEvent += UserNotAuthenticationListener;
        authenticated.ErrorAuthenticationEvent += ErrorAuthenticationListener;

        authorization.UserAuthorizationEvent += UserAuthorizationListener;
        authorization.UserNotAuthorizationEvent += UserNotAuthorizationListener;
        authorization.ErrorAuthorizationEvent += ErrorAuthorizationListener;

        submit.onClick.AddListener(SubmitListener);
    }
    private void Start() {
        if (!PlayerPrefs.HasKey("app_token")) {
            login.placeholder.GetComponent<Text>().text = MultiLanguages.instance.GetString("login");
            password.placeholder.GetComponent<Text>().text = MultiLanguages.instance.GetString("password");
            authorizationPanel.SetActive(true);
        }else {
            authorizationPanel.SetActive(false);

            if (!Helper.CheckConnectNetworking()){
                LoadMainScene();
            }
            else{
                authenticated.StartAuthenticated(PlayerPrefs.GetString("app_token"));
            }
        }
    }


    // ****** Listenrs ******
    // ****** Submit ******
    private void SubmitListener() {
        if (login.text != string.Empty && password.text != string.Empty) {
            authorization.StartAuthorization(login.text, password.text);
        }
        else {
            MessageManager.instance.StartWaitWindow("Все поля должны быть заполнены!");
        }
    }
    // ****** Authorization ******
    private void ErrorAuthorizationListener(object obj) {
        LoaderUI.instance.HideLoaderPanel();
        MessageManager.instance.StartWaitWindow("Не удалось авторизоваться!");
#if UNITY_EDITOR
        Debug.LogError("ErrorAuthorization");
#endif
    }

    private void UserNotAuthorizationListener(object obj) {
#if UNITY_EDITOR
        Debug.Log("UserNotAuthorization");
#endif
        LoaderUI.instance.HideLoaderPanel();
        MessageManager.instance.StartWaitWindow("Не удалось авторизоваться! Проверьте данные, и повторите попытку.");
    }

    private void UserAuthorizationListener(object obj) {
#if UNITY_EDITOR
        Debug.Log("UserAuthorization");
#endif
        ProgressManager.instance.LoadProgress(LoadProgressListener,obj);
        
    }

    private void LoadProgressListener(object obj) {
        authenticated.StartAuthenticated(obj.ToString());
    }

    // ****** Authentication ******
    private void ErrorAuthenticationListener(object obj) {
        LoaderUI.instance.HideLoaderPanel();
        MessageManager.instance.StartWaitWindow("Не удалось авторизоваться!", ResetToken);
#if UNITY_EDITOR
        Debug.LogError("ErrorAuthentication");
#endif
    }

    private void UserNotAuthenticationListener(object obj) {
        LoaderUI.instance.HideLoaderPanel();
        MessageManager.instance.StartWaitWindow("Не удалось авторизоваться!");
#if UNITY_EDITOR
        Debug.Log("UserNotAuthentication");
#endif
    }

    private void UserAuthenticationListener(object obj) {
#if UNITY_EDITOR
        Debug.Log("UserAuthentication");
#endif
        if (bool.Parse(obj.ToString())) {
            if (Helper.CheckConnectNetworking()) {
                Schema.instance.UplloadSchem();
            }
            LoadMainScene(1f);
            //MessageManager.instance.StartWaitWindow("UserAuthentication", LoadMainScene);
        }
        else {
            authorizationPanel.SetActive(true);
        }
    }

    private void LoadMainScene(float delay = 0) {
        StartCoroutine(LoadMainSceneIEnumerator(delay));
    }

    private IEnumerator LoadMainSceneIEnumerator(float delay) {
        yield return new WaitForSeconds(delay);
        SceneManager.LoadScene(MAINSCENE);
    }

    private void ResetToken() {
        if (PlayerPrefs.HasKey("app_token")) {
            PlayerPrefs.DeleteAll();
        }
        SceneManager.LoadScene(0);
    }
}
