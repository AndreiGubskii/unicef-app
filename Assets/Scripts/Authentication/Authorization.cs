﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Authorization : MonoBehaviour {
    public delegate void AuthorizationEvents(object obj);

    public event AuthorizationEvents UserAuthorizationEvent;
    public event AuthorizationEvents UserNotAuthorizationEvent;
    public event AuthorizationEvents ErrorAuthorizationEvent;

    [Serializable]
    public class AuthorizationInfo{
        public string token;
        public string[] non_field_errors;
    }

    IEnumerator AuthorizationIenumerator(string login, string password){
        LoaderUI.instance.ShowLoader();
        WWWForm form = new WWWForm();
        form.AddField("username", login);
        form.AddField("password", password);

        UnityWebRequest www = UnityWebRequest.Post("http://165.227.247.66/api-token-auth/", form);
        yield return www.Send();

        if (www.isHttpError || www.isNetworkError){
        #if UNITY_EDITOR
            Debug.LogError(www.error);
        #endif
            if (ErrorAuthorizationEvent != null){
                ErrorAuthorizationEvent(www.error);
            }
        }else {
            string token = JsonUtility.FromJson<AuthorizationInfo>(www.downloadHandler.text).token;
            string[] errors = JsonUtility.FromJson<AuthorizationInfo>(www.downloadHandler.text).non_field_errors;
            if (token != null){
                PlayerPrefs.SetString("app_token", token);
                if (UserAuthorizationEvent != null){
                    UserAuthorizationEvent(token);
                }
            }else {
                if (UserNotAuthorizationEvent != null){
                    UserNotAuthorizationEvent(errors[0]);
                }
            }
        }
        
    }

    public void StartAuthorization(string login, string password){
        //if (Application.internetReachability == NetworkReachability.NotReachable) return;
        StartCoroutine(AuthorizationIenumerator(login, password));
    }
}
