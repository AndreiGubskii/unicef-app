﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StatisticPanelManager : MonoBehaviour {

    [SerializeField] private Text persName;
    [SerializeField] private Text persAge;
    [SerializeField] private Text persLvl;
    [SerializeField] private Slider healthSlider;
    [SerializeField] private Slider levelSlider;
    [SerializeField] private Text persHealth;
    [SerializeField] private Text persAteCount;
    [SerializeField] private Text persPlayedCount;
    [SerializeField] private Text persWashedCount;
    [SerializeField] private GameObject panel;
    [SerializeField] private Button clousePanel;
    [SerializeField] private Sprite[] sliderSprites;
    
    public static StatisticPanelManager instance;

    private void Awake() {
        instance = this;
    }

    private void Start() {
        clousePanel.onClick.AddListener(ClouseBtnClickListener);
    }

    private void ClouseBtnClickListener() {
        DeactivatePanel();
    }

    public void ActivatePanel(string name,string age, string health, string eatCount, /*string wahCount,*/ string playCount,string lvl) {
        SetPersName(name);
        SetPersAge(age);
        SetPersAte(eatCount);
        SetPlayedCount(playCount);
        /*SetWashCount(wahCount);*/
        SetPersHealth(health);
        SetPersLvl(lvl);
        float h = float.Parse(health);
        if (h>=90) {
            healthSlider.fillRect.GetComponent<Image>().sprite = sliderSprites[0];
        }
        else if(h>=70) {
            healthSlider.fillRect.GetComponent<Image>().sprite = sliderSprites[1];
        }
        else if (h >= 50) {
            healthSlider.fillRect.GetComponent<Image>().sprite = sliderSprites[2];
        }else {
            healthSlider.fillRect.GetComponent<Image>().sprite = sliderSprites[3];
        }
        healthSlider.value = h/100;
        levelSlider.value = MainManager.instance.GetLevelProgress();
        panel.SetActive(true);
    }

    public void ActivatePanel() {
        panel.SetActive(true);
    }

    public void DeactivatePanel() {
        panel.SetActive(false);
    }

    public bool PanelIsActivate() {
        return panel.activeSelf;
    }

    public void SetPersName(string name) {
        this.persName.text = name;
    }

    public void SetPersLvl(string lvl) {
        this.persLvl.text = "Уровень "+lvl;
    }

    public void SetPersAge(string age) {
        int _age = 0;
        int.TryParse(age, out _age);
        this.persAge.text = string.Format("{0} {1}",age,Helper.GetDeclension(_age, "день", "дня", "дней"));
    }

    public void SetPersHealth(string health) {
        this.persHealth.text = health;
    }

    public void SetPersAte(string eatCount) {
        this.persAteCount.text = eatCount;
    }

    public void SetPlayedCount(string playCount) {
        this.persPlayedCount.text = playCount;
    }

    public void SetWashCount(string washCount) {
        this.persWashedCount.text = washCount;
    }
}
