﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DebugSceneManager : MonoBehaviour {
    [SerializeField] private DebugSceneRemover remover;

	
	void Start () {
		GameObject obj = GameObject.Find("Non-destructible");
	    if (obj != null) {
	        Destroy(obj);
	    }
	}
	
	
	void Update () {
	    if (Input.GetKeyUp(KeyCode.Escape)) {
	        ExitGame();
	    }
	}

    public void RemoveAllData() {
        MessageManager.instance.StartConfirmWindow("Хотите удалить все данные?",_RemoveAllData);
    }

    private void _RemoveAllData() {
        try {
            if (remover != null){
                remover.RemovePlayerPrefs();
                remover.RemoveHashData();
                remover.RemoveDataBase();
            }
        }
        catch (Exception e) {
#if UNITY_EDITOR
            Debug.Log(e);
#endif
            MessageManager.instance.StartWaitWindow("Не удалось удалить данные!");
            return;
        }

        MessageManager.instance.StartWaitWindow("Данные удалены успешно!");
    }


    public void ExitGame() {
        MessageManager.instance.StartConfirmWindow("Хотите закрыть приложение?", _ExitGame);
    }

    private void _ExitGame() {
        Application.Quit();
    }


    public void LoadGame() {
        GameObject obj = GameObject.FindObjectOfType<MessageManager>().gameObject;
        if (obj != null) {
            Destroy(obj);
        }
        SceneManager.LoadScene(0);
    }
}
