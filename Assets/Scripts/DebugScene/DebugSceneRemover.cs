﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class DebugSceneRemover : MonoBehaviour {
    private string hashChatPath;
    private string hashDoctorDataPath;
    private string hashFAQDataPath;
    private string dbPath;
    private string schemPath;


    
    private void Start () {
	    hashChatPath = Application.persistentDataPath + "/hashChat.txt";
	    hashDoctorDataPath = Application.persistentDataPath + "/hashDoctorData.txt";
	    hashFAQDataPath = Application.persistentDataPath + "/hashFAQDataPath.txt";
        dbPath = Application.persistentDataPath + "/game_database.db";
        schemPath = Application.persistentDataPath + "/schem.xml";
    }

    public void RemovePlayerPrefs() {
        PlayerPrefs.DeleteAll();
    }

    public void RemoveHashData() {
        if (File.Exists(hashChatPath)) {
            File.Delete(hashChatPath);
        }

        if (File.Exists(hashDoctorDataPath)) {
            File.Delete(hashDoctorDataPath);
        }

        if (File.Exists(hashFAQDataPath)) {
            File.Delete(hashFAQDataPath);
        }

        if (File.Exists(schemPath)) {
            File.Delete(schemPath);
        }
    }

    public void RemoveDataBase() {
        if (File.Exists(dbPath)) {
            File.Delete(dbPath);
        }
    }


}
