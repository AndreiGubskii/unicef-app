﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class ProductsPanelManager : MonoBehaviour {
    public enum ProductsTypes {
        DEFAULT,
        HEAD,
        FACE,
        BODY
        
    }

    public static ProductController head;
    public static ProductController face;
    public static ProductController body;

    public Button switcher;
    [SerializeField] private ScrollRect scrollRect;

    private List<GameObject> objectsList = new List<GameObject>();
    public void Awake() {

        switcher.onClick.AddListener(SwitchPanel);
    }

    private void OnEnable() {
        CreateProducts(DB_Query.instance.GetAllProducts());
    }

    private void OnDisable() {
        StoreManager.instance.skinPanelIsActive = true;
        DestroyAllProducts();
        MainManager.instance.GetPersManager().TakeDressed(DB_Query.instance.GetAllProductsByDressed(1));
        Resources.UnloadUnusedAssets();
    }


    private void CreateProducts(List<DB_Tables.Products> productList) {
        DestroyAllProducts();
        objectsList = new List<GameObject>();
        foreach (var item in productList) {
            GameObject prefab = Resources.Load<GameObject>("Products/store/" + item.name);
            if (prefab == null) continue;  
            GameObject obj = Instantiate(prefab, scrollRect.content);
            ProductController productController = obj.GetComponent<ProductController>();
            productController.Id = item.id;
            productController.Name = item.name;
            productController.Price = item.price;
            productController.Dressed = item.dressed;
            productController.Purchased = item.purchased;
            productController.Code = item.code;
            productController.Type = (ProductsTypes)Enum.Parse(typeof(ProductsTypes), item.type);

            objectsList.Add(obj);

            if (item.dressed == 1) {
                switch (productController.Type) {
                    case ProductsTypes.HEAD:
                        head = productController;
                        break;
                    case ProductsTypes.FACE:
                        face = productController;
                        break;
                    case ProductsTypes.BODY:
                        body = productController;
                        break;
                    default:
                        break;
                }
            }
        }
    }

    private void DestroyAllProducts() {
        if (objectsList != null && objectsList.Count > 0) {
            foreach (GameObject o in objectsList) {
                Destroy(o);
            }
        }
    }

    public ProductController GetProductByCode(int code) {
        return objectsList.First(m => m.GetComponent<ProductController>().Code == code).GetComponent<ProductController>();
    }

    private void SwitchPanel() {
        StoreManager.instance.OpenPanel();
    }
}
