﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class FillerStore : MonoBehaviour {

    private DB_Tables.Products[] products = {
        new DB_Tables.Products("Колпак",5,0,0,10, ProductsPanelManager.ProductsTypes.HEAD.ToString()),
        new DB_Tables.Products("Шапка",3,0,0,11, ProductsPanelManager.ProductsTypes.HEAD.ToString()),
        new DB_Tables.Products("Шляпа_1",2,0,0,12, ProductsPanelManager.ProductsTypes.HEAD.ToString()),
        new DB_Tables.Products("Шляпа_2",5,0,0,4, ProductsPanelManager.ProductsTypes.HEAD.ToString()),
        new DB_Tables.Products("Маска",5,0,0,5, ProductsPanelManager.ProductsTypes.FACE.ToString()),
        new DB_Tables.Products("Очки_1",2,0,0,6, ProductsPanelManager.ProductsTypes.FACE.ToString()),
        new DB_Tables.Products("Очки_2",5,0,0,7, ProductsPanelManager.ProductsTypes.FACE.ToString()),
       /* new DB_Tables.Products("Очки_3",5,0,0, ProductsPanelManager.ProductsTypes.FACE.ToString()),*/
        new DB_Tables.Products("Шарф",3,0,0,8, ProductsPanelManager.ProductsTypes.BODY.ToString()),
        new DB_Tables.Products("Значок",2,0,0,9, ProductsPanelManager.ProductsTypes.BODY.ToString())
    };

    private DB_Tables.Skins[] skins = {
        new DB_Tables.Skins("Кот", 0, 1, 0, 1),
        new DB_Tables.Skins("Собака", 10, 0, 0, 2),
        new DB_Tables.Skins("Барс", 15, 0, 0, 3)
    };

    private IEnumerator Start () {

        //Ожедание пока не создастся бд
        bool products = false;
        bool skins =false;
        while (!products && !skins) {
            yield return new WaitForEndOfFrame();
            products = DB_Query.instance.TableExist("products");
            skins = DB_Query.instance.TableExist("skins");
        }

		FillProducts();
        FillSkins();
	}

    //Заполняет таблицу с товарами в бд 
    private void FillProducts() {
        List<DB_Tables.Products> productList = DB_Query.instance.GetAllProducts();
        if (productList.Count < products.Length){
            DB_Query.instance.SetProducts(products.ToList());
            //CreateProducts(products.ToList());
        }
    }

    //Заполняет таблицу со скинами в бд 
    private void FillSkins() {
        List<DB_Tables.Skins> productList = DB_Query.instance.GetAllSkins();
        if (productList.Count < skins.Length){
            DB_Query.instance.SetSkins(skins.ToList());
        }
    }
}
