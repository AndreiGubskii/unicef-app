﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class SkinController : MonoBehaviour,IPointerClickHandler{

    private string _name;
    public string Name {
        get { return _name; }
        set {
            _name = value;
        }
    }
    private int _price;
    public int Price {
        get { return _price; }
        set {
            priceLabel.GetComponentInChildren<Text>().text = value.ToString();
            _price = value;
        }
    }

    private int _purchased;
    public int Purchased {
        get { return _purchased; }
        set { _purchased = value; }
    }

    private int _dressed;
    public int Dressed{
        get { return _dressed; }
        set { _dressed = value; }
    }

    private int _id;
    public int Id {
        get { return _id; }
        set { _id = value; }
    }

    private int _code;
    public int Code {
        get { return _code; }
        set { _code = value; }
    }
    [SerializeField] private GameObject border;
    [SerializeField] private GameObject buy;
    [SerializeField] private GameObject priceLabel;

    private void Start() {
        SetButton();
    }


    public void SetButton() {
        if (Purchased <= 0) {
            border.SetActive(false);
            priceLabel.SetActive(true);
            buy.gameObject.SetActive(true);
        }
        else if(Dressed > 0) {
            TakeSkinButton();
        }
        else {
            TakeOffSkinButton();
        }
    }

    public void UpdateValues() {
        DB_Tables.Skins skin = DB_Query.instance.SelectSkins(Id);
        this.Name = skin.name;
        this.Price = skin.price;
        this.Dressed = skin.dressed;
        this.Purchased = skin.purchased;
    }

    public void TakeSkin(int code) {
        DB_Query.instance.DressedSkin(code, 1);
        SkinsPanelManager.instance.TakeOffExcept(code);
        UpdateValues();
        SetButton();
        MainManager.instance.ChangePers(Name);
    }

    public void TakeOffSkin(int code) {
        DB_Query.instance.DressedSkin(code, 0);
        UpdateValues();
        SetButton();
    }

    public void TakeOffSkin(){
        DB_Query.instance.DressedSkin(Code, 0);
        UpdateValues();
        SetButton();
    }

    private void TakeSkinButton() {
        border.gameObject.SetActive(true);
        buy.gameObject.SetActive(false);
        priceLabel.SetActive(false);
    }

    private void TakeOffSkinButton() {
        border.gameObject.SetActive(false);
        buy.gameObject.SetActive(false);
        priceLabel.SetActive(false);
    }

    public void OnPointerClick(PointerEventData eventData) {
        if (Purchased <= 0) {
            MessageManager.instance.StartConfirmWindow(String.Format("Купить скин {0}?",Name),BuySkin);
        }
        else if (Dressed <= 0) {
            //take
            this.TakeSkin(this.Code);
        }
    }

    private void BuySkin() {
        if (DB_Query.instance.TakeAwayCoin(this._price,null)){
            DB_Query.instance.BuySkin(this.Code);
            UpdateValues();
            SetButton();
            AudioManager.instance.PlaySound(
                AudioSourcesEnums.PERS,
                SoundEnums.PURCHASE
            );
        }
    }
}
