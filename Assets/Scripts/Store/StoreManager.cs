﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StoreManager : MonoBehaviour {
    [SerializeField] private GameObject productsPanel;
    [SerializeField] private GameObject skinsPanel;
    [SerializeField] private Button openPanelBtn;
    [SerializeField] private Button clousePanelBtn;
    public bool skinPanelIsActive;
    public static StoreManager instance;
    private void Awake() {
        instance = this;
        openPanelBtn.onClick.AddListener(OpenPanelListener);
    }

    private void Start() {
        GetCoinsLabel().text = DB_Query.instance.GetCoins();
        clousePanelBtn.onClick.AddListener(ClouseAllPanels);
        skinPanelIsActive = true;
    }

    private void ClouseAllPanels() {
        productsPanel.SetActive(false);
        skinsPanel.SetActive(false);
        clousePanelBtn.gameObject.SetActive(false);
        CoinsManager.instance.UpdateCoins();
    }

    private void OpenPanelListener() {
        clousePanelBtn.gameObject.SetActive(true);
        OpenPanel();
    }

    public void OpenPanel() {
        if (skinPanelIsActive) {
            productsPanel.SetActive(true);
            skinsPanel.SetActive(false);
            skinPanelIsActive = false;
        }
        else {
            skinsPanel.SetActive(true);
            productsPanel.SetActive(false);
            skinPanelIsActive = true;
        }
    }

    public Text GetCoinsLabel() {
        return openPanelBtn.GetComponentInChildren<Text>();
    }
}
