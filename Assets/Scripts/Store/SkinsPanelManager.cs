﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class SkinsPanelManager : MonoBehaviour {

    public Button switcher;
    [SerializeField] private ScrollRect scrollRect;

    private List<GameObject> objectsList = new List<GameObject>();


    public static SkinsPanelManager instance;
    private void Awake() {
        instance = this;

        switcher.onClick.AddListener(SwitchPanel);
    }

    private void SwitchPanel() {
        StoreManager.instance.OpenPanel();
    }

    private void OnEnable() {
        CreateSkins(DB_Query.instance.GetAllSkins());
    }

    private void OnDisable() {
        StoreManager.instance.skinPanelIsActive = false;
    }

    private void CreateSkins(List<DB_Tables.Skins> skinsList) {
        DestroyAllProducts();
        objectsList = new List<GameObject>();
        foreach (var item in skinsList) {
            GameObject prefab = Resources.Load<GameObject>("Skins/icons/" + item.name);
            if (prefab == null) continue;  
            GameObject obj = Instantiate(prefab, scrollRect.content);

            SkinController skinController = obj.GetComponent<SkinController>();
            skinController.Id = item.id;
            skinController.Name = item.name;
            skinController.Price = item.price;
            skinController.Dressed = item.dressed;
            skinController.Purchased = item.purchased;
            skinController.Code = item.code;

            objectsList.Add(obj);

            
        }
    }

    private void DestroyAllProducts() {
        if (objectsList != null && objectsList.Count > 0) {
            foreach (GameObject o in objectsList) {
                Destroy(o);
            }
        }
    }

    public SkinController GetSkinsByCode(int code) {
        return objectsList.First(m => m.GetComponent<SkinController>().Code == code).GetComponent<SkinController>();
    }

    //Убираем все скины кроме выбранного
    public void TakeOffExcept(int code) {
        GameObject[] objs = objectsList.FindAll(m=>m.GetComponent<SkinController>().Code!=code).ToArray();
        foreach (var o in objs) {
            o.GetComponent<SkinController>().TakeOffSkin();
        }
    }
}
