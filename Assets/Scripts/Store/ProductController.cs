﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ProductController : MonoBehaviour,IPointerClickHandler {
    private string _name;
    public string Name {
        get { return _name; }
        set {
            _name = value;
        }
    }
    private int price;
    public int Price {
        get { return price; }
        set {
            priceLabel.GetComponentInChildren<Text>().text = value.ToString();
            price = value;
        }
    }

    private int _purchased;
    public int Purchased {
        get { return _purchased; }
        set { _purchased = value; }
    }

    private int _dressed;
    public int Dressed{
        get { return _dressed; }
        set { _dressed = value; }
    }

    private ProductsPanelManager.ProductsTypes _type;
    public ProductsPanelManager.ProductsTypes Type{
        get { return _type; }
        set { _type = value; }
    }

    private int _id;

    public int Id {
        get { return _id; }
        set { _id = value; }
    }

    private int _code;
    public int Code {
        get { return _code; }
        set { _code = value; }
    }
    [SerializeField] private GameObject border;
    [SerializeField] private GameObject buy;
    [SerializeField] private GameObject priceLabel;

    private void Start() {
        SetButton();
    }


    public void SetButton() {
        if (Purchased <= 0) {
            border.SetActive(false);
            priceLabel.SetActive(true);
            buy.gameObject.SetActive(true);
        }
        else if(Dressed > 0) {
            TakeDressButton();
        }
        else {
            TakeOffDressButton();
        }
    }

    public void UpdateValues() {
        DB_Tables.Products product = DB_Query.instance.SelectProduct(Code);
        this.Name = product.name;
        this.Price = product.price;
        this.Dressed = product.dressed;
        this.Purchased = product.purchased;
/*        this.Type = (StorePanelManager.ProductsTypes)Enum.Parse(typeof(StorePanelManager.ProductsTypes), product.type);*/
    }

    public void TakeDress(int code) {
        DB_Query.instance.DressedProducts(code, 1);
        UpdateValues();
        SetButton();
    }

    public void TakeOffDress(int code, ProductsPanelManager.ProductsTypes type) {
        DB_Query.instance.DressedProducts(code, 0);
        MainManager.instance.GetPersManager().TakeOffDress(type);
        UpdateValues();
        SetButton();
    }

    private void TakeDressButton() {
        border.gameObject.SetActive(true);
        buy.gameObject.SetActive(false);
        priceLabel.SetActive(false);
    }

    private void TakeOffDressButton() {
        border.gameObject.SetActive(false);
        buy.gameObject.SetActive(false);
        priceLabel.SetActive(false);
    }

    public void OnPointerClick(PointerEventData eventData) {
        if (Purchased <= 0) {
            MessageManager.instance.StartConfirmWindow(String.Format("Купить эту вещь?"), BuyProduct);
        }
        else if (Dressed <= 0) {
            //take
            switch (this.Type){
                case ProductsPanelManager.ProductsTypes.HEAD:
                    if (ProductsPanelManager.head == null){
                        ProductsPanelManager.head = this;
                        this.TakeDress(this.Code);
                    }
                    else{
                        ProductsPanelManager.head.TakeOffDress(ProductsPanelManager.head.Code, ProductsPanelManager.head.Type);
                        ProductsPanelManager.head = this;
                        this.TakeDress(this.Code);
                    }
                    break;
                case ProductsPanelManager.ProductsTypes.FACE:
                    if (ProductsPanelManager.face == null){
                        ProductsPanelManager.face = this;
                        this.TakeDress(this.Code);
                    }
                    else{
                        ProductsPanelManager.face.TakeOffDress(ProductsPanelManager.face.Code, ProductsPanelManager.face.Type);
                        ProductsPanelManager.face = this;
                        this.TakeDress(this.Code);
                    }
                    break;
                case ProductsPanelManager.ProductsTypes.BODY:
                    if (ProductsPanelManager.body == null){
                        ProductsPanelManager.body = this;
                        this.TakeDress(this.Code);
                    }
                    else{
                        ProductsPanelManager.body.TakeOffDress(ProductsPanelManager.body.Code, ProductsPanelManager.body.Type);
                        ProductsPanelManager.body = this;
                        this.TakeDress(this.Code);
                    }
                    break;
                case ProductsPanelManager.ProductsTypes.DEFAULT:
                    break;
            }
        }
        else {
            //takeoff
            TakeOffDress(this.Code, this.Type);
        }
        
    }

    private void BuyProduct() {
        if (DB_Query.instance.TakeAwayCoin(this.price,null)){
            DB_Query.instance.BuyProducts(this.Code);
            UpdateValues();
            SetButton();
            AudioManager.instance.PlaySound(
                AudioSourcesEnums.PERS,
                SoundEnums.PURCHASE
            );
        }
    }
}
