﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FoodController : MonoBehaviour {
    private Mover _mover;
    private Animator _animator;
    [SerializeField] private float animationSpeed;

    private void Awake() {
        _mover = GetComponent<Mover>();
        _animator = GetComponent<Animator>();
        
    }

    private void Start() {
        _mover.InPlaceEvent += InPlaceListener;
        _animator.speed = animationSpeed;
        
        _mover.MoveTo(GameObject.Find("mouse").GetComponent<RectTransform>().anchoredPosition, 200);
    }

    private void InPlaceListener() {
        Destroy(gameObject,0.5f);
    }
}
