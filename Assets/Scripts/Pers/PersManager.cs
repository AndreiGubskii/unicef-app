﻿ using System;
 using System.Collections;
using System.Collections.Generic;
using UnityEngine;
 using UnityEngine.UI;

public class PersManager : MonoBehaviour {
    [System.Serializable]
    public class PersTransform {
        public Transform head;
        public Transform face;
        public Transform body;
    }
    [SerializeField]
    private PersTransform _persTransforms;

    private DB_Tables.PersTable pers;
    private PersAnimationController animationController;

    private bool dead = false;
    private void Awake() {
        animationController = GetComponent<PersAnimationController>();
    }

    private void Start() {
        UpdateValues();

        TakeDressed(DB_Query.instance.GetAllProductsByDressed(1));
    }

    private void Update() {
        if (PersIsDead() && GetPersValues().health > 10) {
            SetDead(false);
        }
        else if(!PersIsDead() && GetPersValues().health <= 10) {
            SetDead(true);
        }
    }

    public void Eat() {
        MainManager.instance.ComplateTask(Tasks.TasksType.EAT);
        GetAnimationController().PlayEatAnimation();
        Debug.Log("I'm eating! nyam nyam nyam!");
    }
    public void Play() {
        MainManager.instance.ComplateTask(Tasks.TasksType.PLAY);
        GetAnimationController().PlayStrokeAnimation();
        Debug.Log("I'm playing!");
    }

    public void Shower() {
        MainManager.instance.ComplateTask(Tasks.TasksType.WASH);
        GetAnimationController().PlayEatAnimation();
        Debug.Log("I'm go to shower");
    }

    public void AddAge() {
        DateTime birth = GetPersValues().birth;
        int age = (DateTime.Now - birth).Days;
        if (age < 1) {
            age = 1;
        }
        DB_Query.instance.SetAge(age);
        UpdateValues();
    }

    public void AddHealth() {
        UpdateValues();
        if (GetPersValues().health < 100) {
            DB_Query.instance.SetHealth(pers.health + 1);
        }
    }

    public void AddHealth(int value) {
        UpdateValues();
        if (GetPersValues().health < 100) {
            int h = pers.health + value;
            if (h > 100) {
                h = 100;
            }
            DB_Query.instance.SetHealth(h);
        }
    }

    public void TakeAwayHealth() {
        UpdateValues();
        if (GetPersValues().health > 0) {
            DB_Query.instance.SetHealth(pers.health - 1);
        }
    }

    public void TakeAwayHealth(int value) {
        UpdateValues();
        if (GetPersValues().health > 0) {
            int h = pers.health - value;
            if (h < 0) {
                h = 0;
            }
            DB_Query.instance.SetHealth(h);
        }
    }

    public DB_Tables.PersTable GetPersValues() {
        UpdateValues();
        return this.pers == null ? (this.pers = DB_Query.instance.SelectPers()) : this.pers;
    }

    public void UpdateValues() {
        this.pers = DB_Query.instance.SelectPers();
    }

    public PersAnimationController GetAnimationController() {
        return animationController;
    }

    public void CancelStrokeAnimationListener(){
        SenderDataManager.instance.SendData();
    }

    public void CancelEatAnimationListener() {
        SenderDataManager.instance.SendData();
    }

    public void SetDead(bool dead) {
        this.dead = dead;
        GetAnimationController().PlayDeadAnimation(dead);
    }

    public bool PersIsDead() {
        return this.dead;
    }

    public void TakeDressed(List<DB_Tables.Products> products) {
        foreach (var product in products) {
            Transform _transform = new RectTransform();
            GameObject child = null;

            switch ((ProductsPanelManager.ProductsTypes) Enum.Parse(typeof(ProductsPanelManager.ProductsTypes),
                product.type)) {
                case ProductsPanelManager.ProductsTypes.HEAD:
                    _transform = _persTransforms.head;
                    if (_persTransforms.head.childCount > 0) {
                        child = _persTransforms.head.GetChild(0).gameObject;
                    }
                    break;
                case ProductsPanelManager.ProductsTypes.FACE:
                    _transform = _persTransforms.face;
                    if (_persTransforms.face.childCount > 0) {
                        child = _persTransforms.face.GetChild(0).gameObject;
                    }
                    break;
                case ProductsPanelManager.ProductsTypes.BODY:
                    _transform = _persTransforms.body;
                    if (_persTransforms.body.childCount > 0) {
                        child = _persTransforms.body.GetChild(0).gameObject;
                    }
                    break;
                case ProductsPanelManager.ProductsTypes.DEFAULT:
                    break;
                default:
                    break;
            }
            if (child != null) {
                Destroy(child);
            }
            GameObject o = Resources.Load<GameObject>("Products/pers/" + product.name);
            if (o != null) {
                Instantiate(o, _transform);
            }
            
        }
    }

    

    public void TakeOffDress(ProductsPanelManager.ProductsTypes type) {
        GameObject child = null;
        switch (type) {
                case ProductsPanelManager.ProductsTypes.HEAD:
                    if (_persTransforms.head.childCount > 0) {
                        child = _persTransforms.head.GetChild(0).gameObject;
                    }
                    break;
                case ProductsPanelManager.ProductsTypes.FACE:
                    if (_persTransforms.face.childCount > 0) {
                        child = _persTransforms.face.GetChild(0).gameObject;
                    }
                    break;
                case ProductsPanelManager.ProductsTypes.BODY:
                    if (_persTransforms.body.childCount > 0) {
                        child = _persTransforms.body.GetChild(0).gameObject;
                    }
                    break;
                case ProductsPanelManager.ProductsTypes.DEFAULT:
                    break;
                default:
                    break;
            }
            if (child != null) {
                Destroy(child);
            }
    }

}
