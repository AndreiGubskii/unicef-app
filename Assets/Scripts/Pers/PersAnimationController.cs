﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class PersAnimationController : MonoBehaviour {

    private int idle_01 = Animator.StringToHash("idle_01");
    private int idle_02 = Animator.StringToHash("idle_02");
    private int idle_03 = Animator.StringToHash("idle_03");
    private int idle_04 = Animator.StringToHash("idle_04");
    private int idle_05 = Animator.StringToHash("idle_05");
    private int eat = Animator.StringToHash("eat");
    private int stroke = Animator.StringToHash("stroking");
    private float timer = 0;
    private int[] animationsId;
    private int[] animationsPlayedId;
    private Animator _animator;
    private bool deadActive;
	void Awake () {
	    _animator = GetComponent<Animator>();
	    animationsId = new[] {idle_01,idle_02,idle_03,idle_04,idle_05};
	    animationsPlayedId = new int[2];
        
	}

    private void Start() {
        
    }

    // Update is called once per frame
	void Update () {
        if(deadActive) return;
		if(timer <= 0){
            PlayRandomAnimation();
	    }
		else {
		    timer -= 1*Time.deltaTime;
		}
	}

    public void PlayAnimation(int id) {
        _animator.SetTrigger(id);
    }

    public void PlayRandomAnimation() {
        int id = Random.Range(0, animationsId.Length);
        while (animationsPlayedId.Contains(animationsId[id])) {
            id = Random.Range(0, animationsId.Length);
        }
        PlayAnimation(animationsId[id]);
        animationsPlayedId[animationsPlayedId.Length - 1] = animationsPlayedId[0];
        animationsPlayedId[0] = animationsId[id];
        timer = 5f;

    }

    public void PlayEatAnimation() {
        _animator.SetTrigger(eat);
        AudioManager.instance.PlaySound(
            AudioSourcesEnums.PERS,
            SoundEnums.EATING,
            1
            );
    }

    public void PlayStrokeAnimation() {
        _animator.SetTrigger(stroke);
        if (DB_Query.instance.GetAllSkinsByDressed(1).Exists(m => m.name == "Собака")) {
            AudioManager.instance.PlaySound(
                AudioSourcesEnums.PERS,
                SoundEnums.CARESS_DOG
            );
        }else {
            AudioManager.instance.PlaySound(
                AudioSourcesEnums.PERS,
                SoundEnums.CARESS
            );
        }
        
    }

    public void PlayDeadAnimation(bool play) {
        deadActive = play;
        _animator.SetBool("dead",play);
    }
}
