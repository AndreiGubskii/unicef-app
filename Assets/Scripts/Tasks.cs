﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Tasks : MonoBehaviour {
    public enum TasksType {
        DEFAULT,
        PLAY,
        EAT,
        WASH
    }
    public enum TskStatus {
        DEFAULT,
        WAIT,
        COMPLATE,
        MISSED,
        TOOK_AWAY_HEALTH
    }
    //Массив нотификаций
    public static DB_Tables.TasksTable[] tasks = new[] {
        new DB_Tables.TasksTable(
            TasksType.EAT,
            MultiLanguages.instance.GetString("notification", Tasks.TasksType.EAT.ToString(), "title"),
            MultiLanguages.instance.GetString("notification", Tasks.TasksType.EAT.ToString(), "message")),
        new DB_Tables.TasksTable(
            TasksType.PLAY,
            MultiLanguages.instance.GetString("notification", Tasks.TasksType.PLAY.ToString(), "title"),
            MultiLanguages.instance.GetString("notification", Tasks.TasksType.PLAY.ToString(), "message")),
        new DB_Tables.TasksTable(
            TasksType.WASH,
            MultiLanguages.instance.GetString("notification", Tasks.TasksType.WASH.ToString(), "title"),
            MultiLanguages.instance.GetString("notification", Tasks.TasksType.WASH.ToString(), "message"))
    };
    //Возвращает нотификации по имени
    public static DB_Tables.TasksTable GetNotificationByName(TasksType name) {
        foreach (DB_Tables.TasksTable task in tasks) {
            if (task.task == name) {
                return task;
            }
        }
        return null;
    }
    //Возвращает редко используемую таску
    public static TasksType GetRareTask(List<DB_Tables.TasksTable> tasksTable) {
        Dictionary<Tasks.TasksType, int> tasks = new Dictionary<Tasks.TasksType, int>();
        tasks.Add(Tasks.TasksType.EAT, 0);
        tasks.Add(Tasks.TasksType.PLAY, 0);
        //Исключил таску wash
        //tasks.Add(Tasks.TasksType.WASH, 0);
        foreach (DB_Tables.TasksTable task in tasksTable) {
            switch (task.task) {
                case Tasks.TasksType.EAT:
                    tasks[Tasks.TasksType.EAT]++;
                    break;
                case Tasks.TasksType.PLAY:
                    tasks[Tasks.TasksType.PLAY]++;
                    break;
                case Tasks.TasksType.WASH:
                    tasks[Tasks.TasksType.WASH]++;
                    break;
                default:
                    break;
            }
        }
        
        return tasks.OrderBy(kvp => kvp.Value).First().Key;
    }
}
