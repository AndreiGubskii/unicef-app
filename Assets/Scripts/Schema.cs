﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Xml;
using UnityEngine;
using UnityEngine.Networking;
using System.Xml.Linq;
using System.IO;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class Schema : MonoBehaviour {

    [Serializable]
    public class SchemaInfo{
        public string taking_interval;
        public string taking_time;
        public string drug_id;
    }

    public static Schema instance;
    private string patht;
    private List<DB_Tables.TasksTable> tasks = new List<DB_Tables.TasksTable>();
    private void Awake() {
        instance = this;
        patht = Application.persistentDataPath + "/schem.xml";

        if (!File.Exists(patht)){
            File.Create(patht);
        }
    }

    private IEnumerator Start(){
        yield return new WaitForEndOfFrame();
        List<DB_Tables.TasksTable> tasks = DB_Query.instance.SelectTaskByStatus(Tasks.TskStatus.WAIT);
        
        foreach (DB_Tables.TasksTable task in tasks) {
            if (task.time.AddHours(Helper.GetTimeActiveTask(task.repeat_time)) < DateTime.Now) {
                DB_Query.instance.SetSatus(Tasks.TskStatus.MISSED, DateTime.Now, task.id);
            }
        }
    }
    private IEnumerator UplloadSchemIenumerator(string token) {
        if (Helper.CheckConnectNetworking()) {
            UnityWebRequest www = UnityWebRequest.Get("http://165.227.247.66/schema/");
            www.SetRequestHeader("Authorization", "Token " + token);
            yield return www.Send();
            if (www.isNetworkError) {
            #if UNITY_EDITOR
                Debug.Log(www.error);
            #endif

            }
            else {
                XElement schemRoot = new XElement("root");
                List<string> schemeList = GetSchemList(www.downloadHandler.text);
#if UNITY_EDITOR
                Debug.Log("Schem is loaded!");
#endif
                if (schemeList.Count > 0) {
                    foreach (string s1 in schemeList) {
                        XElement drug = new XElement("drug");
                        XElement id = new XElement("id", JsonUtility.FromJson<SchemaInfo>(s1).drug_id);
                        XElement interval = new XElement("interval",
                            JsonUtility.FromJson<SchemaInfo>(s1).taking_interval);
                        XElement time = new XElement("time", JsonUtility.FromJson<SchemaInfo>(s1).taking_time);
                        drug.Add(id);
                        drug.Add(interval);
                        drug.Add(time);
                        schemRoot.Add(drug);
                    }
                    File.WriteAllText(patht, schemRoot.ToString());

                }
            }
        }
        else {
            //MessageManager.instance.StartWaitWindow("No connection!",UplloadSchem);
        }
    }


    public void UplloadSchem() {
        StartCoroutine(UplloadSchemIenumerator(PlayerPrefs.GetString("app_token")));
    }

    public List<string> GetSchemList(string json) {
        string s = json.Remove(0, 1);
        s = s.Remove(s.Length - 1, 1);
        bool save = false;
        string ss = "";
        List<string> sList = new List<string>();
        foreach (char c in s) {
            if (c == '{') {
                save = true;
            }else if(c == '}') {
                save = false;
                ss += c;
                sList.Add(ss);
                ss = "";
            }
            if (save) {
                ss += c;
            }
        }
        return sList;
    }

    public void LoadXmlSchem() {
        XmlDocument schemXml = new XmlDocument();
        string fileText = File.ReadAllText(patht);
        tasks = DB_Query.instance.SelectTaskByStatus(Tasks.TskStatus.WAIT);
        string title = "";
        string message = "";
        
        try {
            schemXml.LoadXml(fileText);
        }
        catch (Exception e) {
#if UNITY_EDITOR
            Debug.Log(e);
#endif
            MessageManager.instance.StartWaitWindow("Не удалось загрузить расписание - уведомлений!");
            return;
        }
        
        foreach (XmlNode node in schemXml.SelectNodes("root/drug")) {
            string id = "", interval = "";
            DateTime time = DateTime.Now;
            for (int i = 0; i < node.ChildNodes.Count; i++) {
                switch (node.ChildNodes[i].Name) {
                    case "id":
                        id = node.ChildNodes[i].InnerXml;
                        break;
                    case "interval":
                        interval = node.ChildNodes[i].InnerXml;
                        break;
                    case "time":
                        time = Convert.ToDateTime(node.ChildNodes[i].InnerXml, CultureInfo.CurrentCulture);
                        while (time < DateTime.Now) {
                            time = time.AddHours(int.Parse(interval));
                        }
                        break;
                    default:
                        break;
                }
            }
            bool timeExist = false;
            if (tasks.Count > 0) {
                foreach (DB_Tables.TasksTable task in tasks) {
                    if (time == task.time) {
                        title = task.title;
                        message = task.message;
                        //if (!MainManager.GameOnPause) {
                            LocalNotification.StartNotification(int.Parse(id), time, int.Parse(interval), title, message);
                        //}
                        
                        timeExist = true;
                    }
                }
                if (!timeExist) {
                    CreateNewNotification(int.Parse(id), time, int.Parse(interval));
                }
            }
            else {
                CreateNewNotification(int.Parse(id), time, int.Parse(interval));
            }
        }
        //MessageManager.instance.StartWaitWindow("Нотификации установлены успешно!");
        MainManager.instance.SetTimer();
        MainManager.GameOnPause = true;
    }

    private void CreateNewNotification(int id,DateTime time,int interval) {
        DB_Tables.TasksTable notification = Tasks.GetNotificationByName(Tasks.GetRareTask(tasks));
        //if (!MainManager.GameOnPause) {
            LocalNotification.StartNotification(id, time, interval, notification.title, notification.message);
        //}
        DB_Query.instance.SetTask(notification.task, notification.title, notification.message, Tasks.TskStatus.WAIT, time,id,interval);
        tasks = DB_Query.instance.SelectTaskByStatus(Tasks.TskStatus.WAIT);
        MainManager.instance.SetTimer();
        MainManager.GameOnPause = true;
    }
}
