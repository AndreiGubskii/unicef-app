﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SetNamePanelManager : MonoBehaviour {

    [SerializeField] private InputField inputName;
    [SerializeField] private Button submit;

    private void OnEnable() {
        inputName.onEndEdit.AddListener(InputeNameListener);
        submit.onClick.AddListener(ClickSubmitListener);
    }
    private void OnDisable() {
        inputName.onEndEdit.RemoveListener(InputeNameListener);
        submit.onClick.RemoveListener(ClickSubmitListener);
    }

    private void InputeNameListener(string arg0){
        
    }

    private void ClickSubmitListener() {
        if (inputName.text == "" || inputName.text.Length < 3) {
            MessageManager.instance.StartWaitWindow("Имя питомца должно быть более 3 букв!");
            return;
        }
        else {
            DB_Query.instance.SetPers(inputName.text);
            MainManager.instance.SetNameLabel(inputName.text);
            this.gameObject.SetActive(false);
        }
    }
}
