﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Reflection.Emit;
using UnityEngine;
using UnityEngine.Networking;

public class ProgressManager : MonoBehaviour {
    [System.Serializable]
    public class ProgressModel {
        public string name;
        public int level;
        public int health;
        public int happiness;
        public int fed;
        public int caress;
        public int coins;
        public int age;

        public int[] skins;
        public int[] decor;
        public int[] bought_skins;
        public int[] bought_decor;
    }

    [System.Serializable]
    public class Progress {
        public ProgressModel[] progress;
    }

    public static ProgressManager instance;

    private void Awake() {
        instance = this;
    }

    private void Start() {
    }

    public void SaveProgress() {

        DB_Tables.PersTable pers = DB_Query.instance.SelectPers();
        int fed = pers.ate;
        int caress = pers.played;
        DB_Tables.Skins dressedSkins = DB_Query.instance.GetAllSkinsByDressed(1).First();
        List<DB_Tables.Products> decors = DB_Query.instance.GetAllProductsByDressed(1);

        List<DB_Tables.Skins> boughtSkins = DB_Query.instance.GetAllSkinsByPurchased(1);
        List<DB_Tables.Products> boughtDecors = DB_Query.instance.GetAllProductsByPurchased(1);

        StartCoroutine(SaveProgressIenumerator(
            pers.name,
            pers.level,
            pers.age,
            pers.health,
            (int)pers.happiness,
            fed,
            caress,
            pers.coins,
            dressedSkins,
            decors,
            boughtSkins,
            boughtDecors
        ));
    }

    private IEnumerator SaveProgressIenumerator(string name,int level,int age,int health,int happiness, int fed,int caress,int coins, DB_Tables.Skins skins, List<DB_Tables.Products> decors,List<DB_Tables.Skins> boughtSkins,List<DB_Tables.Products> boughtProducts)
    {
        Dictionary<string, string> headers = new Dictionary<string, string>();

        headers.Add("ContentType", "application/x-www-form-urlencoded");
        headers.Add("Authorization", "Token " + PlayerPrefs.GetString("app_token"));

        WWWForm form = new WWWForm();

        form.AddField("name", name);
        form.AddField("level", level);
        form.AddField("age", age);
        form.AddField("health", health);
        form.AddField("happiness", happiness);
        form.AddField("fed", fed);
        form.AddField("caress", caress);
        form.AddField("coins", coins);
        form.AddField("skins", skins.id);
        
        foreach (DB_Tables.Products decor in decors) {
            form.AddField("decor", decor.code);
        }

        foreach (DB_Tables.Skins s in boughtSkins) {
            form.AddField("bought_skins", s.code);
        }

        foreach (DB_Tables.Products p in boughtProducts) {
            form.AddField("bought_decor", p.code);
        }

        WWW www = new WWW("http://165.227.247.66/progress/", form.data,headers);
        yield return www;

        if (www.isDone){
        #if UNITY_EDITOR
            Debug.Log("Progress saved!");
        #endif
            
            if (!string.IsNullOrEmpty(www.error)) {
                #if UNITY_EDITOR
                    Debug.LogError(www.error);
                #endif

            }
        }
    }

    public delegate void ProgressManagreDelegate(object obj);
    
    public void LoadProgress(ProgressManagreDelegate callback,object obj) {
        StartCoroutine(LoadProgressIEnumerator(callback,obj));
    }

    private IEnumerator LoadProgressIEnumerator(ProgressManagreDelegate callback, object obj = null) {
        Dictionary<string, string> headers = new Dictionary<string, string>();

        headers.Add("ContentType", "application/x-www-form-urlencoded");
        headers.Add("Authorization", "Token " + PlayerPrefs.GetString("app_token"));

        WWW www = new WWW("http://165.227.247.66/progress/", null,headers);
        yield return www;

        if (www.isDone && string.IsNullOrEmpty(www.error)){
            Progress p = JsonUtility.FromJson<Progress>("{\"progress\":"+www.text+"}");
            if (p.progress.Length != 0) {
                ProgressModel progress = p.progress.Last();

                DB_Query.instance.SetPers(
                    progress.name,
                    progress.age,
                    progress.level,
                    progress.health,
                    progress.happiness,
                    progress.coins,
                    DateTime.Now.AddDays(-progress.age),
                    progress.fed,
                    progress.caress);


                DB_Query.instance.DressedSkin(progress.skins.First(), 1);

                foreach (var decor in progress.decor) {
                    DB_Query.instance.DressedProducts(decor, 1);
                }

                foreach (var decor in progress.bought_decor) {
                    DB_Query.instance.BuyProducts(decor);
                }

                foreach (var skin in progress.bought_skins) {
                    DB_Query.instance.BuySkin(skin);
                }
            }
        }
        else {
        #if UNITY_EDITOR
            Debug.LogError(www.error);
        #endif

        }

        if (callback != null) {
            callback(obj);
        }
    }

    //Сохраняем статус при сворачивании прилы
    void OnApplicationPause(bool pauseStatus){
        if (pauseStatus) {
            SaveProgress();
        }
    }


}
