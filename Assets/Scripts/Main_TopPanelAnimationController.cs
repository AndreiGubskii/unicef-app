﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Main_TopPanelAnimationController : MonoBehaviour {

    private Animator _animator;
    public static Main_TopPanelAnimationController instance;

    private void Awake() {
        instance = this;
    }

    private Animator GetAnimator () {
	    return _animator == null ? _animator = GetComponent<Animator>() : _animator;
	}

    public void PlayHappiessAnimation() {
        GetAnimator().SetTrigger("happiess");
        AudioManager.instance.PlaySound(
            AudioSourcesEnums.PERS,
            SoundEnums.HAPPINESS
        );
    }

    public void AnimationEndListener() {
        CoinsManager.instance.AddCoins(1);
        AudioManager.instance.PlaySound(
            AudioSourcesEnums.PERS,
            SoundEnums.GETCOIN
        );
    }
}
