﻿using System.Collections;
using System.Collections.Generic;
using System.Xml;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MultiLanguages : MonoBehaviour {

    public static MultiLanguages instance;
    private XmlDocument xmlDoc;
    private void Awake() {
        instance = this;
    }

    void Start () {
        TextAsset textAsset = Resources.Load<TextAsset>("languages");
        xmlDoc = new XmlDocument();
        xmlDoc.LoadXml(textAsset.text);

    }
	
    public string GetString(string param) {
        XmlNode node = xmlDoc.SelectSingleNode(string.Format("languages/{0}/{1}/{2}",GetLanguge(),SceneManager.GetActiveScene().name,param));
        if (node != null) {
            return node.InnerText;
        }
        return "Empty node";
    }

    public string GetString(string parrent,string name,string param){
        XmlNode node = xmlDoc.SelectSingleNode(string.Format("languages/{0}/{1}/{2}/{3}", GetLanguge(), parrent,name,param));
        if (node != null){
            return node.InnerText;
        }
        return "Empty node";
    }

    public string GetString(string parrent,string param){
        XmlNode node = xmlDoc.SelectSingleNode(string.Format("languages/{0}/{1}/{2}", GetLanguge(), parrent,param));
        if (node != null){
            return node.InnerText;
        }
        return "Empty node";
    }

    private string GetLanguge() {
        switch (Application.systemLanguage) {
            case SystemLanguage.Russian:
                return "Russian";
            case SystemLanguage.English:
                return "English";
            default:
                return "Russian";
        }
    }
}
