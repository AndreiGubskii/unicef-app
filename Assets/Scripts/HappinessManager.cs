﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

class HappinessManager : MonoBehaviour {
    private Slider slider;
    public static HappinessManager instance;

    private void Awake() {
        instance = this;
    }

    private void Start() {
        UpdateValue();
    }

    public void UpdateValue() {
        if (DateTime.Today.Date != Helper.LastDateOpenApp) {
            Helper.LastDateOpenApp = DateTime.Today.Date;
            Helper.HappinessCounter = 0;
        }
        
        // В слайдер только дробную часть
        GetSlider().value = DB_Query.instance.GetHappiness() - (int) DB_Query.instance.GetHappiness();

        if (Helper.HappinessFirst < (int) DB_Query.instance.GetHappiness()) {
            Main_TopPanelAnimationController.instance.PlayHappiessAnimation();
            Helper.HappinessFirst = (int) DB_Query.instance.GetHappiness();
        }
    }

    public Slider GetSlider() {
        return slider == null ? (slider = GetComponent<Slider>()) : slider;
    }
}

