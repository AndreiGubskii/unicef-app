﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NonDestructible : MonoBehaviour {
	void Start () {
		DontDestroyOnLoad(this);
	}
}
