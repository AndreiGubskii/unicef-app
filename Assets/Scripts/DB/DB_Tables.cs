﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DB_Tables : MonoBehaviour {
    [System.Serializable]
    public class TasksTable{
        public TasksTable() {
        }

        public TasksTable(Tasks.TasksType task,string title,string message) {
            this.task = task;
            this.title = title;
            this.message = message;
        }

        public int id;
        public int drug_id;
        public Tasks.TasksType task;
        public string title;
        public string message;
        public Tasks.TskStatus status;
        public DateTime time;
        public DateTime lead_time;
        public int sended;
        public int repeat_time;
    }

    [System.Serializable]
    public class PersTable{
        public PersTable(){
        }

        public PersTable(string name, int age, int health, DateTime birth,int level) {
            this.name = name;
            this.age = age;
            this.health = health;
            this.birth = birth;
            this.level = level;
        }
        public int id;
        public string name;
        public int age;
        public int health;
        public DateTime birth;
        public int level;
        public int coins;
        public int played;
        public int ate;
        public float happiness;
    }

    [System.Serializable]
    public class Products {
        public Products() {
        }

        public Products(string name, int price, int purchased, int dressed,int code, string type) {
            this.name = name;
            this.price = price;
            this.purchased = purchased;
            this.dressed = dressed;
            this.code = code;
            this.type = type;
        }

        public int id;
        public string name;
        public int price;
        public int purchased;
        public int dressed;
        public int code;
        public string type;
    }

    [System.Serializable]
    public class Skins {
        public Skins() {
        }
        public Skins(string name, int price, int purchased, int dressed,int code) {
            this.name = name;
            this.price = price;
            this.purchased = purchased;
            this.dressed = dressed;
            this.code = code;
        }
        public int id;
        public string name;
        public int price;
        public int purchased;
        public int dressed;
        public int code;
    }
}
