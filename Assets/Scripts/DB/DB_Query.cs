﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Runtime.InteropServices.ComTypes;
using Mono.Data.Sqlite;
using UnityEngine;

public class DB_Query : MonoBehaviour {

    private IDbConnection dbconn;
    private string dbPath;
    private string taskTable = "tasks";
    private string persTable = "pers";
    private string productsTable = "products";
    private string skinsTable = "skins";
    public static DB_Query instance;


    private string hashChatPath;
    private string hashDoctorDataPath;
    private string hashFAQDataPath;
    private void Awake() {
        instance = this;
        dbPath = "URI=file:" + Application.persistentDataPath + "/game_database.db"; //Path to database.

        if (!File.Exists(dbPath.Replace("URI=file:", ""))) {
            File.Create(dbPath.Replace("URI=file:",""));
        }

        hashChatPath = Application.persistentDataPath + "/hashChat.txt";
        hashDoctorDataPath = Application.persistentDataPath + "/hashDoctorData.txt";
        hashFAQDataPath = Application.persistentDataPath + "/hashFAQDataPath.txt";

    }

    private void Start() {
        CreateTasksTable();
        CreatePersTable();
        CreateProductsTable();
        CreateSkinsTable();

        if (!File.Exists(hashChatPath)){
            File.Create(hashChatPath);
        }
        if (!File.Exists(hashDoctorDataPath)){
            File.Create(hashDoctorDataPath);
        }
        if (!File.Exists(hashFAQDataPath)){
            File.Create(hashFAQDataPath);
        }
    }

    //Проверяем существует ли таблица
    public bool TableExist (string tableName) {
        dbconn = (IDbConnection)new SqliteConnection(dbPath);
        dbconn.Open(); //Open connection to the database.
        IDbCommand dbcmd = dbconn.CreateCommand();
        string sqlQuery = string.Format("SELECT name FROM sqlite_master WHERE type = 'table' AND name = '{0}'", tableName);
        dbcmd.CommandText = sqlQuery;

        IDataReader reader = dbcmd.ExecuteReader();
        string name = "";
        while (reader.Read()){
            name = reader["name"].ToString();

        }
        reader.Close();
        reader = null;
        dbcmd.Dispose();
        dbcmd = null;
        dbconn.Close();
        dbconn = null;
        return name != "";
    }

    //Создает таблицу уведомлений
    public void CreateTasksTable(){
        dbconn = (IDbConnection)new SqliteConnection(dbPath);
        dbconn.Open();
        IDbCommand dbcmd = dbconn.CreateCommand();
        string sqlQuery = string.Format("create table if not exists "+taskTable+ " (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,drug_id INTEGER, task STRING(256),title STRING(256),message STRING(256), status STRING(256),time STRING(256),lead_time STRING(256),sended INTEGER,repeat_time INTEGER);");
        dbcmd.CommandText = sqlQuery;
        IDataReader reader = dbcmd.ExecuteReader();
        reader.Close();
        reader = null;
        dbcmd.Dispose();
        dbcmd = null;
        dbconn.Close();
        dbconn = null;
    }
   
    // Записывает таску в базу данных
    public void SetTask(Tasks.TasksType task,string title,string message, Tasks.TskStatus status,DateTime time,int drug_id,int repeat_time) {
        dbconn = (IDbConnection)new SqliteConnection(dbPath);
        dbconn.Open();
        IDbCommand dbcmd = dbconn.CreateCommand();
        string sqlQuery = string.Format("INSERT INTO " + taskTable + " (task,title,message,status,time,lead_time,drug_id,sended,repeat_time) values('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}')", task, title, message,status,time,DateTime.Now, drug_id,0, repeat_time);
        dbcmd.CommandText = sqlQuery;

        IDataReader reader = dbcmd.ExecuteReader();
        reader.Close();
        reader = null;
        dbcmd.Dispose();
        dbcmd = null;
        dbconn.Close();
        dbconn = null;
    }
    //Возвращает массив задач выбранных по статусу
    public List<DB_Tables.TasksTable> SelectTaskByStatus(Tasks.TskStatus status){
        dbconn = (IDbConnection)new SqliteConnection(dbPath);
        dbconn.Open(); //Open connection to the database.
        IDbCommand dbcmd = dbconn.CreateCommand();
        string sqlQuery = string.Format("SELECT id,task,title,message,status,time,lead_time,drug_id,sended,repeat_time FROM " + taskTable+" WHERE status='{0}'", status);
        dbcmd.CommandText = sqlQuery;

        IDataReader reader = dbcmd.ExecuteReader();
        List<DB_Tables.TasksTable> tasks = new List<DB_Tables.TasksTable>();
        while (reader.Read()){
            DB_Tables.TasksTable task = new DB_Tables.TasksTable();
            task.id = int.Parse(reader["id"].ToString());
            task.repeat_time = int.Parse(reader["repeat_time"].ToString());
            task.drug_id = int.Parse(reader["drug_id"].ToString());
            task.task = (Tasks.TasksType)Enum.Parse(typeof(Tasks.TasksType), reader["task"].ToString());
            task.title = reader["title"].ToString();
            task.message = reader["message"].ToString();
            task.status = (Tasks.TskStatus)Enum.Parse(typeof(Tasks.TskStatus), reader["status"].ToString());
            task.time = Convert.ToDateTime(reader["time"].ToString());
            task.lead_time = Convert.ToDateTime(reader["lead_time"].ToString());
            task.sended = int.Parse(reader["sended"].ToString());
            tasks.Add(task);
        }
        reader.Close();
        reader = null;
        dbcmd.Dispose();
        dbcmd = null;
        dbconn.Close();
        dbconn = null;
        return tasks;
    }
    //Возвращает массив задач выбранных по статусу
    public List<DB_Tables.TasksTable> SelectTaskTookAwayHealth(){
        dbconn = (IDbConnection)new SqliteConnection(dbPath);
        dbconn.Open(); //Open connection to the database.
        IDbCommand dbcmd = dbconn.CreateCommand();
        string sqlQuery = string.Format("SELECT * FROM " + taskTable+" WHERE status='TOOK_AWAY_HEALTH' ORDER BY time DESC");
        dbcmd.CommandText = sqlQuery;
        IDataReader reader = dbcmd.ExecuteReader();
        List<DB_Tables.TasksTable> tasks = new List<DB_Tables.TasksTable>();
        while (reader.Read()){
            DB_Tables.TasksTable task = new DB_Tables.TasksTable();
            task.id = int.Parse(reader["id"].ToString());
            task.repeat_time = int.Parse(reader["repeat_time"].ToString());
            task.drug_id = int.Parse(reader["drug_id"].ToString());
            task.task = (Tasks.TasksType)Enum.Parse(typeof(Tasks.TasksType), reader["task"].ToString());
            task.title = reader["title"].ToString();
            task.message = reader["message"].ToString();
            task.status = (Tasks.TskStatus)Enum.Parse(typeof(Tasks.TskStatus), reader["status"].ToString());
            task.time = Convert.ToDateTime(reader["time"].ToString());
            task.lead_time = Convert.ToDateTime(reader["lead_time"].ToString());
            task.sended = int.Parse(reader["sended"].ToString());
            tasks.Add(task);
        }
        reader.Close();
        reader = null;
        dbcmd.Dispose();
        dbcmd = null;
        dbconn.Close();
        dbconn = null;
        return tasks;
    }
    //Возвращает массив задач выбранных по имени 
    public List<DB_Tables.TasksTable> SelectTaskByName(Tasks.TasksType name, Tasks.TskStatus status){
        dbconn = (IDbConnection)new SqliteConnection(dbPath);
        dbconn.Open(); //Open connection to the database.
        IDbCommand dbcmd = dbconn.CreateCommand();
        string sqlQuery = string.Format("SELECT * FROM " + taskTable+" WHERE task='{0}' and status='{1}'", name, status);
        dbcmd.CommandText = sqlQuery;

        IDataReader reader = dbcmd.ExecuteReader();
        List<DB_Tables.TasksTable> tasks = new List<DB_Tables.TasksTable>();
        while (reader.Read()){
            DB_Tables.TasksTable task = new DB_Tables.TasksTable();
            task.id = int.Parse(reader["id"].ToString());
            task.repeat_time = int.Parse(reader["repeat_time"].ToString());
            task.drug_id = int.Parse(reader["drug_id"].ToString());
            task.task = (Tasks.TasksType)Enum.Parse(typeof(Tasks.TasksType), reader["task"].ToString());
            task.title = reader["title"].ToString();
            task.message = reader["message"].ToString();
            task.status = (Tasks.TskStatus)Enum.Parse(typeof(Tasks.TskStatus), reader["status"].ToString());
            task.time = Convert.ToDateTime(reader["time"].ToString());
            task.lead_time = Convert.ToDateTime(reader["lead_time"].ToString());
            task.sended = int.Parse(reader["sended"].ToString());
            tasks.Add(task);
        }
        reader.Close();
        reader = null;
        dbcmd.Dispose();
        dbcmd = null;
        dbconn.Close();
        dbconn = null;
        return tasks;
    }
    //Возвращает массив задач выбранных по значению sended (0 или 1)
    public List<DB_Tables.TasksTable> SelectTaskBySentStatus(int status){
        dbconn = (IDbConnection)new SqliteConnection(dbPath);
        dbconn.Open(); //Open connection to the database.
        IDbCommand dbcmd = dbconn.CreateCommand();
        string sqlQuery = string.Format("SELECT * FROM " + taskTable+ " WHERE sended={0} AND (status='COMPLATE' OR status='TOOK_AWAY_HEALTH')", status);
        dbcmd.CommandText = sqlQuery;
        IDataReader reader = dbcmd.ExecuteReader();
        List<DB_Tables.TasksTable> tasks = new List<DB_Tables.TasksTable>();
        while (reader.Read()){
            DB_Tables.TasksTable task = new DB_Tables.TasksTable();
            task.id = int.Parse(reader["id"].ToString());
            task.repeat_time = int.Parse(reader["repeat_time"].ToString());
            task.drug_id = int.Parse(reader["drug_id"].ToString());
            task.task = (Tasks.TasksType)Enum.Parse(typeof(Tasks.TasksType), reader["task"].ToString());
            task.title = reader["title"].ToString();
            task.message = reader["message"].ToString();
            task.status = (Tasks.TskStatus)Enum.Parse(typeof(Tasks.TskStatus), reader["status"].ToString());
            task.time = Convert.ToDateTime(reader["time"].ToString());
            task.lead_time = Convert.ToDateTime(reader["lead_time"].ToString());
            task.sended = int.Parse(reader["sended"].ToString());
            tasks.Add(task);
        }
        reader.Close();
        reader = null;
        dbcmd.Dispose();
        dbcmd = null;
        dbconn.Close();
        dbconn = null;
        return tasks;
    }
    //Меняет статус задаче
    public void SetSatus(Tasks.TskStatus status,int id){
        dbconn = (IDbConnection)new SqliteConnection(dbPath);
        dbconn.Open();
        IDbCommand dbcmd = dbconn.CreateCommand();
        string sqlQuery = string.Format("UPDATE " + taskTable + " SET status = '{0}' WHERE id={1}", status,id);
        dbcmd.CommandText = sqlQuery;

        IDataReader reader = dbcmd.ExecuteReader();
        reader.Close();
        reader = null;
        dbcmd.Dispose();
        dbcmd = null;
        dbconn.Close();
        dbconn = null;
    }
    //Меняет статус задаче и сохраняет время изменения
    public void SetSatus(Tasks.TskStatus status,DateTime leadTime,int id){
        dbconn = (IDbConnection)new SqliteConnection(dbPath);
        dbconn.Open();
        IDbCommand dbcmd = dbconn.CreateCommand();
        string sqlQuery = string.Format("UPDATE " + taskTable + " SET status='{0}',lead_time='{1}' WHERE id={2}", status,leadTime,id);
        dbcmd.CommandText = sqlQuery;

        IDataReader reader = dbcmd.ExecuteReader();
        reader.Close();
        reader = null;
        dbcmd.Dispose();
        dbcmd = null;
        dbconn.Close();
        dbconn = null;
    }
    //Обновляет время изменения таски
    public void UpdateTimeLead(DateTime leadTime,int id){
        dbconn = (IDbConnection)new SqliteConnection(dbPath);
        dbconn.Open();
        IDbCommand dbcmd = dbconn.CreateCommand();
        string sqlQuery = string.Format("UPDATE " + taskTable + " SET lead_time='{0}' WHERE id={1}", leadTime,id);
        dbcmd.CommandText = sqlQuery;

        IDataReader reader = dbcmd.ExecuteReader();
        reader.Close();
        reader = null;
        dbcmd.Dispose();
        dbcmd = null;
        dbconn.Close();
        dbconn = null;
    }
    //Помечает задачу как выполненую
    public void SetComplateTask(DateTime leadtime, int id){
        dbconn = (IDbConnection)new SqliteConnection(dbPath);
        dbconn.Open();
        IDbCommand dbcmd = dbconn.CreateCommand();
        string sqlQuery = string.Format("UPDATE " + taskTable + " SET status='{2}',lead_time = '{0}' WHERE id={1}", leadtime.ToString("G"), id, Tasks.TskStatus.COMPLATE);
        dbcmd.CommandText = sqlQuery;

        IDataReader reader = dbcmd.ExecuteReader();
        reader.Close();
        reader = null;
        dbcmd.Dispose();
        dbcmd = null;
        dbconn.Close();
        dbconn = null;
    }
     //Помечает задачу как отправленную на сервер
    public void SetSended(int id){
        dbconn = (IDbConnection)new SqliteConnection(dbPath);
        dbconn.Open();
        IDbCommand dbcmd = dbconn.CreateCommand();
        string sqlQuery = string.Format("UPDATE " + taskTable + " SET sended = 1 WHERE id={0}",id);
        dbcmd.CommandText = sqlQuery;

        IDataReader reader = dbcmd.ExecuteReader();
        reader.Close();
        reader = null;
        dbcmd.Dispose();
        dbcmd = null;
        dbconn.Close();
        dbconn = null;
    }

    /******* Pers *******/

    //Создает таблицу персонажа
    public void CreatePersTable(){
        dbconn = (IDbConnection)new SqliteConnection(dbPath);
        dbconn.Open();
        IDbCommand dbcmd = dbconn.CreateCommand();
        string sqlQuery = string.Format("create table if not exists " + persTable + " (" +
                                        "id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL," +
                                        " name STRING(256)," +
                                        "age INTEGER," +
                                        "health INTEGER," +
                                        "birth STRING(256)," +
                                        "level INTEGER," +
                                        "coins INTEGER," +
                                        "happiness STRING(256)," +
                                        "ate INTEGER," +
                                        "played INTEGER" +
                                        ");");
        dbcmd.CommandText = sqlQuery;
        IDataReader reader = dbcmd.ExecuteReader();
        reader.Close();
        reader = null;
        dbcmd.Dispose();
        dbcmd = null;
        dbconn.Close();
        dbconn = null;
    }

    //Смена имени персонажа
    public void SetName(string name){
        dbconn = (IDbConnection)new SqliteConnection(dbPath);
        dbconn.Open();
        IDbCommand dbcmd = dbconn.CreateCommand();
        string sqlQuery = string.Format("UPDATE {1} SET name='{0}' WHERE id=1", name,persTable);
        dbcmd.CommandText = sqlQuery;

        IDataReader reader = dbcmd.ExecuteReader();
        reader.Close();
        reader = null;
        dbcmd.Dispose();
        dbcmd = null;
        dbconn.Close();
        dbconn = null;
    }
    //Смена возвраста персонажа
    public void SetAge(int age){
        dbconn = (IDbConnection)new SqliteConnection(dbPath);
        dbconn.Open();
        IDbCommand dbcmd = dbconn.CreateCommand();
        string sqlQuery = string.Format("UPDATE {1} SET age='{0}' WHERE id=1", age,persTable);
        dbcmd.CommandText = sqlQuery;

        IDataReader reader = dbcmd.ExecuteReader();
        reader.Close();
        reader = null;
        dbcmd.Dispose();
        dbcmd = null;
        dbconn.Close();
        dbconn = null;
    }
    //Смена здоровья персонажа
    public void SetHealth(int health){
        dbconn = (IDbConnection)new SqliteConnection(dbPath);
        dbconn.Open();
        IDbCommand dbcmd = dbconn.CreateCommand();
        string sqlQuery = string.Format("UPDATE {1} SET health='{0}' WHERE id=1", health,persTable);
        dbcmd.CommandText = sqlQuery;

        IDataReader reader = dbcmd.ExecuteReader();
        reader.Close();
        reader = null;
        dbcmd.Dispose();
        dbcmd = null;
        dbconn.Close();
        dbconn = null;
    }
    //Смена уровня
    public void SetLevel(int level){
        dbconn = (IDbConnection)new SqliteConnection(dbPath);
        dbconn.Open();
        IDbCommand dbcmd = dbconn.CreateCommand();
        string sqlQuery = string.Format("UPDATE {1} SET level='{0}' WHERE id=1", level,persTable);
        dbcmd.CommandText = sqlQuery;

        IDataReader reader = dbcmd.ExecuteReader();
        reader.Close();
        reader = null;
        dbcmd.Dispose();
        dbcmd = null;
        dbconn.Close();
        dbconn = null;
    }

    //Счетчик кормешки
    public void SetEat(int count){
        dbconn = (IDbConnection)new SqliteConnection(dbPath);
        dbconn.Open();
        IDbCommand dbcmd = dbconn.CreateCommand();
        string sqlQuery = string.Format("UPDATE {1} SET ate='{0}' WHERE id=1", count,persTable);
        dbcmd.CommandText = sqlQuery;

        IDataReader reader = dbcmd.ExecuteReader();
        reader.Close();
        reader = null;
        dbcmd.Dispose();
        dbcmd = null;
        dbconn.Close();
        dbconn = null;
    }

    //Счетчик игры
    public void SetPlayed(int count){
        dbconn = (IDbConnection)new SqliteConnection(dbPath);
        dbconn.Open();
        IDbCommand dbcmd = dbconn.CreateCommand();
        string sqlQuery = string.Format("UPDATE {1} SET played='{0}' WHERE id=1", count,persTable);
        dbcmd.CommandText = sqlQuery;

        IDataReader reader = dbcmd.ExecuteReader();
        reader.Close();
        reader = null;
        dbcmd.Dispose();
        dbcmd = null;
        dbconn.Close();
        dbconn = null;
    }

    //Вызывается один раз при наименовании питомца и задаются значения по умолчанию
    public void SetPers(string name){
        dbconn = (IDbConnection)new SqliteConnection(dbPath);
        dbconn.Open();
        IDbCommand dbcmd = dbconn.CreateCommand();
        string sqlQuery = string.Format("INSERT INTO " + persTable + " (id,name,age,health,birth,level,coins,happiness,ate,played) values(1,'{0}',{1},{2},'{3}',{4},{5},{6},{7},{8})", name,1,100,DateTime.Now,1,200,0,0,0);
        dbcmd.CommandText = sqlQuery;

        IDataReader reader = dbcmd.ExecuteReader();
        reader.Close();
        reader = null;
        dbcmd.Dispose();
        dbcmd = null;
        dbconn.Close();
        dbconn = null;
    }

    public void SetPers(string name, int age, int level, int health, int happiness, int coins,DateTime birth,int ate,int played)
    {
        dbconn = (IDbConnection)new SqliteConnection(dbPath);
        dbconn.Open();
        IDbCommand dbcmd = dbconn.CreateCommand();
        string sqlQuery = string.Format("INSERT OR REPLACE INTO " + persTable + " (id,name,age,health,birth,level,coins,happiness,ate,played) values(1,'{0}',{1},{2},'{3}',{4},{5},{6},{7},{8})", name, age, health, birth, level, coins, happiness,ate,played);
        dbcmd.CommandText = sqlQuery;

        IDataReader reader = dbcmd.ExecuteReader();
        reader.Close();
        reader = null;
        dbcmd.Dispose();
        dbcmd = null;
        dbconn.Close();
        dbconn = null;
    }

    //Берем все данные перса
    public DB_Tables.PersTable SelectPers(){
        dbconn = (IDbConnection)new SqliteConnection(dbPath);
        dbconn.Open(); //Open connection to the database.
        IDbCommand dbcmd = dbconn.CreateCommand();
        string sqlQuery = string.Format("SELECT * FROM {0}",persTable);
        dbcmd.CommandText = sqlQuery;

        IDataReader reader = dbcmd.ExecuteReader();
        DB_Tables.PersTable pers = new DB_Tables.PersTable();
        while (reader.Read()){
            try {
                pers.id = int.Parse(reader["id"].ToString());
                pers.name = reader["name"].ToString();
                pers.age = int.Parse(reader["age"].ToString());
                pers.health = int.Parse(reader["health"].ToString());
                pers.birth = Convert.ToDateTime(reader["birth"].ToString());
                pers.level = int.Parse(reader["level"].ToString());
                pers.coins = int.Parse(reader["coins"].ToString());
                pers.played = int.Parse(reader["played"].ToString());
                pers.ate = int.Parse(reader["ate"].ToString());
                pers.happiness = float.Parse(reader["happiness"].ToString());
            }
            catch (Exception e) {
#if UNITY_EDITOR
                Debug.Log(e);
#endif
                pers = new DB_Tables.PersTable();
            }
            
        }
        reader.Close();
        reader = null;
        dbcmd.Dispose();
        dbcmd = null;
        dbconn.Close();
        dbconn = null;
        return pers;
    }
    public delegate void DB_Query_Callback();
    //Добавляем монеты
    public void AddCoin (int countCoins, DB_Query_Callback callback) {
        int _coins = MainManager.instance.GetPersManager().GetPersValues().coins + countCoins;
        dbconn = (IDbConnection)new SqliteConnection(dbPath);
        dbconn.Open();
        IDbCommand dbcmd = dbconn.CreateCommand();
        string sqlQuery = string.Format("UPDATE {1} SET coins={0} WHERE id=1", _coins, persTable);
        dbcmd.CommandText = sqlQuery;

        IDataReader reader = dbcmd.ExecuteReader();
        reader.Close();
        reader = null;
        dbcmd.Dispose();
        dbcmd = null;
        dbconn.Close();
        dbconn = null;
        if (callback != null) {
            callback();
        }
    }

    //Отнимаем монеты
    public bool TakeAwayCoin (int countCoins, DB_Query_Callback callback) {
        int _coins = MainManager.instance.GetPersManager().GetPersValues().coins - countCoins;
        if (_coins < 0) {
            MessageManager.instance.StartWaitWindow("Не достаточно монет!");
            return false;
        }
        dbconn = (IDbConnection)new SqliteConnection(dbPath);
        dbconn.Open();
        IDbCommand dbcmd = dbconn.CreateCommand();
        string sqlQuery = string.Format("UPDATE {1} SET coins={0} WHERE id=1", _coins, persTable);
        dbcmd.CommandText = sqlQuery;

        IDataReader reader = dbcmd.ExecuteReader();
        reader.Close();
        reader = null;
        dbcmd.Dispose();
        dbcmd = null;
        dbconn.Close();
        dbconn = null;
        if (callback != null){
            callback();
        }

        return true;
    }

    //Вернуть количество монет
    public string GetCoins(){
        dbconn = (IDbConnection)new SqliteConnection(dbPath);
        dbconn.Open(); //Open connection to the database.
        IDbCommand dbcmd = dbconn.CreateCommand();
        string sqlQuery = string.Format("SELECT coins FROM {0}",persTable);
        dbcmd.CommandText = sqlQuery;

        IDataReader reader = dbcmd.ExecuteReader();
        string coin = "";
        while (reader.Read()){
            coin = reader["coins"].ToString();

        }
        reader.Close();
        reader = null;
        dbcmd.Dispose();
        dbcmd = null;
        dbconn.Close();
        dbconn = null;
        return coin;
    }

    //Добавляем счастья
    public void AddHappiness (float value) {
        value = 0.1f;
        if(Helper.HappinessCounter >= 2) return;

        Helper.HappinessCounter++;

        float _happiness = MainManager.instance.GetPersManager().GetPersValues().happiness + value;
        dbconn = (IDbConnection)new SqliteConnection(dbPath);
        dbconn.Open();
        IDbCommand dbcmd = dbconn.CreateCommand();
        string sqlQuery = string.Format("UPDATE {1} SET happiness='{0}' WHERE id=1", _happiness, persTable);
        dbcmd.CommandText = sqlQuery;

        IDataReader reader = dbcmd.ExecuteReader();
        reader.Close();
        reader = null;
        dbcmd.Dispose();
        dbcmd = null;
        dbconn.Close();
        dbconn = null;

    }

    //Вернуть счастья
    public float GetHappiness(){
        dbconn = (IDbConnection)new SqliteConnection(dbPath);
        dbconn.Open(); //Open connection to the database.
        IDbCommand dbcmd = dbconn.CreateCommand();
        string sqlQuery = string.Format("SELECT happiness FROM {0}",persTable);
        dbcmd.CommandText = sqlQuery;

        IDataReader reader = dbcmd.ExecuteReader();
        float happiness = 0;
        while (reader.Read()){
            happiness = float.Parse(reader["happiness"].ToString());
        }
        reader.Close();
        reader = null;
        dbcmd.Dispose();
        dbcmd = null;
        dbconn.Close();
        dbconn = null;
        return happiness;
    }

    /* ****** Магазин ***** */

    //Создает таблицу товаров
    public void CreateProductsTable(){
        dbconn = (IDbConnection)new SqliteConnection(dbPath);
        dbconn.Open();
        IDbCommand dbcmd = dbconn.CreateCommand();
        string sqlQuery = string.Format("create table if not exists "+productsTable+ " (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, name STRING(256), price INTEGER, purchased INTEGER,dressed INTEGER,code INTEGER,type STRING(256));");
        dbcmd.CommandText = sqlQuery;
        IDataReader reader = dbcmd.ExecuteReader();
        reader.Close();
        reader = null;
        dbcmd.Dispose();
        dbcmd = null;
        dbconn.Close();
        dbconn = null;
    }

    //Получить все товары магазина
    public List<DB_Tables.Products> GetAllProducts() {
        dbconn = (IDbConnection)new SqliteConnection(dbPath);
        dbconn.Open(); //Open connection to the database.
        IDbCommand dbcmd = dbconn.CreateCommand();
        string sqlQuery = string.Format("SELECT * FROM {0}", productsTable);
        dbcmd.CommandText = sqlQuery;

        IDataReader reader = dbcmd.ExecuteReader();
        List<DB_Tables.Products> productsList = new List<DB_Tables.Products>();
        while (reader.Read()){
            DB_Tables.Products products = new DB_Tables.Products();
            try{
                products.id = int.Parse(reader["id"].ToString());
                products.name = reader["name"].ToString();
                products.type = reader["type"].ToString();
                products.purchased = int.Parse(reader["purchased"].ToString());
                products.price = int.Parse(reader["price"].ToString());
                products.dressed = int.Parse(reader["dressed"].ToString());
                products.code = int.Parse(reader["code"].ToString());
                
                productsList.Add(products);
            }catch (Exception e){
                Debug.Log(e.Message);
            }
        }
        reader.Close();
        reader = null;
        dbcmd.Dispose();
        dbcmd = null;
        dbconn.Close();
        dbconn = null;
        
        return productsList;
    }
    //Получаем конкретный товар по code
    public DB_Tables.Products SelectProduct(int code){
        dbconn = (IDbConnection)new SqliteConnection(dbPath);
        dbconn.Open(); //Open connection to the database.
        IDbCommand dbcmd = dbconn.CreateCommand();
        string sqlQuery = string.Format("SELECT * FROM {0} WHERE code={1}",productsTable,code);
        dbcmd.CommandText = sqlQuery;

        IDataReader reader = dbcmd.ExecuteReader();
        DB_Tables.Products product = new DB_Tables.Products();
        while (reader.Read()){
            try {
                product.id = int.Parse(reader["id"].ToString());
                product.name = reader["name"].ToString();
                product.type = reader["type"].ToString();
                product.purchased = int.Parse(reader["purchased"].ToString());
                product.price = int.Parse(reader["price"].ToString());
                product.code = int.Parse(reader["code"].ToString());
                product.dressed = int.Parse(reader["dressed"].ToString());
            }
            catch (Exception e) {
                Debug.Log(e.Message);
            }
            
        }
        reader.Close();
        reader = null;
        dbcmd.Dispose();
        dbcmd = null;
        dbconn.Close();
        dbconn = null;
        return product;
    }

    //Получаем все товары по свойству dressed
    public List<DB_Tables.Products> GetAllProductsByDressed(int dressed) {
        dbconn = (IDbConnection)new SqliteConnection(dbPath);
        dbconn.Open(); //Open connection to the database.
        IDbCommand dbcmd = dbconn.CreateCommand();
        string sqlQuery = string.Format("SELECT * FROM {0} where dressed={1}", productsTable,dressed);
        dbcmd.CommandText = sqlQuery;

        IDataReader reader = dbcmd.ExecuteReader();
        List<DB_Tables.Products> productsList = new List<DB_Tables.Products>();
        while (reader.Read()){
            DB_Tables.Products products = new DB_Tables.Products();
            try{
                products.id = int.Parse(reader["id"].ToString());
                products.name = reader["name"].ToString();
                products.type = reader["type"].ToString();
                products.purchased = int.Parse(reader["purchased"].ToString());
                products.price = int.Parse(reader["price"].ToString());
                products.code = int.Parse(reader["code"].ToString());
                products.dressed = int.Parse(reader["dressed"].ToString());
                
                productsList.Add(products);
            }catch (Exception e){
                Debug.Log(e.Message);
            }
        }
        reader.Close();
        reader = null;
        dbcmd.Dispose();
        dbcmd = null;
        dbconn.Close();
        dbconn = null;
        
        return productsList;
    }

    //Получаем все товары по свойству dressed
    public List<DB_Tables.Products> GetAllProductsByPurchased(int purchased) {
        dbconn = (IDbConnection)new SqliteConnection(dbPath);
        dbconn.Open(); //Open connection to the database.
        IDbCommand dbcmd = dbconn.CreateCommand();
        string sqlQuery = string.Format("SELECT * FROM {0} where purchased={1}", productsTable, purchased);
        dbcmd.CommandText = sqlQuery;

        IDataReader reader = dbcmd.ExecuteReader();
        List<DB_Tables.Products> productsList = new List<DB_Tables.Products>();
        while (reader.Read()){
            DB_Tables.Products products = new DB_Tables.Products();
            try{
                products.id = int.Parse(reader["id"].ToString());
                products.name = reader["name"].ToString();
                products.type = reader["type"].ToString();
                products.purchased = int.Parse(reader["purchased"].ToString());
                products.price = int.Parse(reader["price"].ToString());
                products.code = int.Parse(reader["code"].ToString());
                products.dressed = int.Parse(reader["dressed"].ToString());
                
                productsList.Add(products);
            }catch (Exception e){
                Debug.Log(e.Message);
            }
        }
        reader.Close();
        reader = null;
        dbcmd.Dispose();
        dbcmd = null;
        dbconn.Close();
        dbconn = null;
        
        return productsList;
    }

    //Купить товар
    public void BuyProducts(int code) {
        dbconn = (IDbConnection)new SqliteConnection(dbPath);
        dbconn.Open();
        IDbCommand dbcmd = dbconn.CreateCommand();
        string sqlQuery = string.Format("UPDATE {0} SET purchased={1} WHERE code={2}", productsTable,1,code);
        dbcmd.CommandText = sqlQuery;

        IDataReader reader = dbcmd.ExecuteReader();
        reader.Close();
        reader = null;
        dbcmd.Dispose();
        dbcmd = null;
        dbconn.Close();
        dbconn = null;
    }

    //Одеть/Снять товар 0-снять 1-одеть
    public void DressedProducts(int code,int value) {
        dbconn = (IDbConnection)new SqliteConnection(dbPath);
        dbconn.Open();
        IDbCommand dbcmd = dbconn.CreateCommand();
        string sqlQuery = string.Format("UPDATE {0} SET dressed={1} WHERE code={2}", productsTable,value,code);
        dbcmd.CommandText = sqlQuery;

        IDataReader reader = dbcmd.ExecuteReader();
        reader.Close();
        reader = null;
        dbcmd.Dispose();
        dbcmd = null;
        dbconn.Close();
        dbconn = null;
    }

    //Записать товары в таблицу
    public void SetProducts(List<DB_Tables.Products> products){
        dbconn = (IDbConnection)new SqliteConnection(dbPath);
        dbconn.Open();
        foreach (var product in products) {
            IDbCommand dbcmd = dbconn.CreateCommand();

            string sqlQuery = string.Format("INSERT OR REPLACE INTO {0} (id,name,price,purchased,dressed,code,type) values((select id from {0} where name = '{1}'),'{1}',{2},{3},{4},{5},'{6}')", 
                productsTable, 
                product.name,
                product.price,
                product.purchased,
                product.dressed,
                product.code,
                product.type);
            dbcmd.CommandText = sqlQuery;
            IDataReader reader = dbcmd.ExecuteReader();
            reader.Close();
            reader = null;
            dbcmd.Dispose();
            dbcmd = null;
        }
        dbconn.Close();
        dbconn = null;
    }


    // ****** Скины *****

    //Создает таблицу скинов
    public void CreateSkinsTable(){
        dbconn = (IDbConnection)new SqliteConnection(dbPath);
        dbconn.Open();
        IDbCommand dbcmd = dbconn.CreateCommand();
        string sqlQuery = string.Format("create table if not exists "+skinsTable+ " (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, name STRING(256), price INTEGER, purchased INTEGER,dressed INTEGER,code INTEGER);");
        dbcmd.CommandText = sqlQuery;
        IDataReader reader = dbcmd.ExecuteReader();
        reader.Close();
        reader = null;
        dbcmd.Dispose();
        dbcmd = null;
        dbconn.Close();
        dbconn = null;
    }

    //Получить все скины магазина
    public List<DB_Tables.Skins> GetAllSkins() {
        dbconn = (IDbConnection)new SqliteConnection(dbPath);
        dbconn.Open(); //Open connection to the database.
        IDbCommand dbcmd = dbconn.CreateCommand();
        string sqlQuery = string.Format("SELECT * FROM {0}", skinsTable);
        dbcmd.CommandText = sqlQuery;

        IDataReader reader = dbcmd.ExecuteReader();
        List<DB_Tables.Skins> productsList = new List<DB_Tables.Skins>();
        while (reader.Read()){
            DB_Tables.Skins skin = new DB_Tables.Skins();
            try{
                skin.id = int.Parse(reader["id"].ToString());
                skin.name = reader["name"].ToString();
                skin.purchased = int.Parse(reader["purchased"].ToString());
                skin.price = int.Parse(reader["price"].ToString());
                skin.dressed = int.Parse(reader["dressed"].ToString());
                skin.code = int.Parse(reader["code"].ToString());
                
                productsList.Add(skin);
            }catch (Exception e){
                Debug.Log(e.Message);
            }
        }
        reader.Close();
        reader = null;
        dbcmd.Dispose();
        dbcmd = null;
        dbconn.Close();
        dbconn = null;
        
        return productsList;
    }
    //Получаем конкретный скин по code
    public DB_Tables.Skins SelectSkins(int code){
        dbconn = (IDbConnection)new SqliteConnection(dbPath);
        dbconn.Open(); //Open connection to the database.
        IDbCommand dbcmd = dbconn.CreateCommand();
        string sqlQuery = string.Format("SELECT * FROM {0} WHERE code={1}", skinsTable, code);
        dbcmd.CommandText = sqlQuery;

        IDataReader reader = dbcmd.ExecuteReader();
        DB_Tables.Skins skin = new DB_Tables.Skins();
        while (reader.Read()){
            try {
                skin.id = int.Parse(reader["id"].ToString());
                skin.name = reader["name"].ToString();
                skin.purchased = int.Parse(reader["purchased"].ToString());
                skin.price = int.Parse(reader["price"].ToString());
                skin.dressed = int.Parse(reader["dressed"].ToString());
                skin.code = int.Parse(reader["code"].ToString());
            }
            catch (Exception e) {
                Debug.Log(e.Message);
            }
            
        }
        reader.Close();
        reader = null;
        dbcmd.Dispose();
        dbcmd = null;
        dbconn.Close();
        dbconn = null;
        return skin;
    }

    //Получаем все скины по свойству dressed
    public List<DB_Tables.Skins> GetAllSkinsByDressed(int dressed) {
        dbconn = (IDbConnection)new SqliteConnection(dbPath);
        dbconn.Open(); //Open connection to the database.
        IDbCommand dbcmd = dbconn.CreateCommand();
        string sqlQuery = string.Format("SELECT * FROM {0} where dressed={1}", skinsTable, dressed);
        dbcmd.CommandText = sqlQuery;

        IDataReader reader = dbcmd.ExecuteReader();
        List<DB_Tables.Skins> productsList = new List<DB_Tables.Skins>();
        while (reader.Read()){
            DB_Tables.Skins skin = new DB_Tables.Skins();
            try{
                skin.id = int.Parse(reader["id"].ToString());
                skin.name = reader["name"].ToString();
                skin.purchased = int.Parse(reader["purchased"].ToString());
                skin.price = int.Parse(reader["price"].ToString());
                skin.dressed = int.Parse(reader["dressed"].ToString());
                skin.code = int.Parse(reader["code"].ToString());
                
                productsList.Add(skin);
            }catch (Exception e){
                Debug.Log(e.Message);
            }
        }
        reader.Close();
        reader = null;
        dbcmd.Dispose();
        dbcmd = null;
        dbconn.Close();
        dbconn = null;
        
        return productsList;
    }

    //Получаем все скины по свойству purchased
    public List<DB_Tables.Skins> GetAllSkinsByPurchased(int purchased) {
        dbconn = (IDbConnection)new SqliteConnection(dbPath);
        dbconn.Open(); //Open connection to the database.
        IDbCommand dbcmd = dbconn.CreateCommand();
        string sqlQuery = string.Format("SELECT * FROM {0} where purchased={1}", skinsTable, purchased);
        dbcmd.CommandText = sqlQuery;

        IDataReader reader = dbcmd.ExecuteReader();
        List<DB_Tables.Skins> productsList = new List<DB_Tables.Skins>();
        while (reader.Read()){
            DB_Tables.Skins skin = new DB_Tables.Skins();
            try{
                skin.id = int.Parse(reader["id"].ToString());
                skin.name = reader["name"].ToString();
                skin.purchased = int.Parse(reader["purchased"].ToString());
                skin.price = int.Parse(reader["price"].ToString());
                skin.dressed = int.Parse(reader["dressed"].ToString());
                skin.code = int.Parse(reader["code"].ToString());
                
                productsList.Add(skin);
            }catch (Exception e){
                Debug.Log(e.Message);
            }
        }
        reader.Close();
        reader = null;
        dbcmd.Dispose();
        dbcmd = null;
        dbconn.Close();
        dbconn = null;
        
        return productsList;
    }
    //Купить скин
    public void BuySkin(int code) {
        dbconn = (IDbConnection)new SqliteConnection(dbPath);
        dbconn.Open();
        IDbCommand dbcmd = dbconn.CreateCommand();
        string sqlQuery = string.Format("UPDATE {0} SET purchased={1} WHERE code={2}", skinsTable,1, code);
        dbcmd.CommandText = sqlQuery;

        IDataReader reader = dbcmd.ExecuteReader();
        reader.Close();
        reader = null;
        dbcmd.Dispose();
        dbcmd = null;
        dbconn.Close();
        dbconn = null;
    }

    //Одеть/Снять скин 0-снять 1-одеть
    public void DressedSkin(int code, int value) {
        dbconn = (IDbConnection)new SqliteConnection(dbPath);
        dbconn.Open();
        IDbCommand dbcmd = dbconn.CreateCommand();
        string sqlQuery = string.Format("UPDATE {0} SET dressed={1} WHERE code={2}", skinsTable,value, code);
        dbcmd.CommandText = sqlQuery;

        IDataReader reader = dbcmd.ExecuteReader();
        reader.Close();
        reader = null;
        dbcmd.Dispose();
        dbcmd = null;
        dbconn.Close();
        dbconn = null;
    }

    //Записать скины в таблицу
    public void SetSkins(List<DB_Tables.Skins> skins){
        dbconn = (IDbConnection)new SqliteConnection(dbPath);
        dbconn.Open();
        foreach (var product in skins) {
            IDbCommand dbcmd = dbconn.CreateCommand();

            string sqlQuery = string.Format("INSERT OR REPLACE INTO {0} (id,name,price,purchased,dressed,code) values((select id from {0} where name = '{1}'),'{1}',{2},{3},{4},{5})", 
                skinsTable, 
                product.name,
                product.price,
                product.purchased,
                product.dressed,
                product.code
                );
            dbcmd.CommandText = sqlQuery;
            IDataReader reader = dbcmd.ExecuteReader();
            reader.Close();
            reader = null;
            dbcmd.Dispose();
            dbcmd = null;
        }
        dbconn.Close();
        dbconn = null;
    }
}
