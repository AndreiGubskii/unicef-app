﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadScene : MonoBehaviour {

    [SerializeField] private float delay;
    [SerializeField] private int sceneNumber;
	private IEnumerator Start () {
		yield return new WaitForSeconds(this.delay);
        SceneManager.LoadScene(sceneNumber);
	}

}
