﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FeedbackPanelManager : MonoBehaviour {
    public Button exitBtn;

    public void Start() {
        exitBtn.onClick.AddListener(ClousePanel);
    }

    private void ClousePanel() {
        this.gameObject.SetActive(false);
    }
}
