﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class MessagePanelManager : MonoBehaviour {

    [System.Serializable]
    public class MessageModel {
        public string message;
        public string attach;
        public string created_at;
        public string answer;
    }

    [System.Serializable]
    public class Messages {
        public MessageModel[] messages;
    }

    [SerializeField] private GameObject outgoingMessage;
    [SerializeField] private GameObject incomingMessage;
    [SerializeField] private ScrollRect scrollRect;
    [SerializeField] private Button submit;
    [SerializeField] private InputField input;
    
    private string filePath;
    public Button exitBtn;

    private List<GameObject> messagesObjects = new List<GameObject>();
    private SystemMenuManager systemMenuManager;
    private void Awake() {
        systemMenuManager = SystemMenuManager.instance;
        filePath = systemMenuManager.HashChatPath;
    }

    private void Start() {
        exitBtn.onClick.AddListener(ClousePanel);
        submit.onClick.AddListener(SubmitClickListener);
        input.onEndEdit.AddListener(InputEndEditListener);
    }

    private void InputEndEditListener(string arg0) {
#if UNITY_ANDROID && (!UNITY_EDITOR_WIN && !UNITY_EDITOR_OSX && !UNITY_STANDALONE_OSX && !UNITY_STANDALONE_WIN)
        SendMessage(input.text);
#endif
    }

    private void SubmitClickListener() {
        SendChatMessage(input.text);
    }

    private void OnEnable() {
        File.WriteAllText(filePath, systemMenuManager.HashChat);
        LoadAllMessages();
    }

    private void LoadAllMessages() {
        StartCoroutine(LoadAllMessagesIEnumerator());
    }

    private IEnumerator LoadAllMessagesIEnumerator() {
        Dictionary<string, string> headers = new Dictionary<string, string>();

        headers.Add("ContentType", "application/x-www-form-urlencoded");
        headers.Add("Authorization", "Token " + PlayerPrefs.GetString("app_token"));

        WWW www = new WWW("http://165.227.247.66/feedback/", null, headers);
        yield return www;

        if (www.isDone && string.IsNullOrEmpty(www.error)){
            string json = "{\"messages\":" + www.text + "}";

            if (systemMenuManager.HashChat != json) {
                systemMenuManager.HashChat = json;
                File.WriteAllText(filePath, systemMenuManager.HashChat);

                AudioManager.instance.PlaySound(
                    AudioSourcesEnums.PERS,
                    SoundEnums.NEWMESSAGE
                );

            }
            else if(systemMenuManager.HashChat == json && messagesObjects.Count != 0) {
                yield return new WaitForSeconds(5);
                LoadAllMessages();
                yield break;
            }
            
            CreateMessagesObjs(json);
        }
        else {
            CreateMessagesObjs(systemMenuManager.HashChat);
            #if UNITY_EDITOR
                Debug.LogError(www.error);
            #endif

            while (!Helper.CheckConnectNetworking()){
                yield return new WaitForEndOfFrame();
            }
        }
        yield return new WaitForEndOfFrame();
        scrollRect.content.anchoredPosition3D = new Vector2(0,
            scrollRect.content.rect.height - 316);

        yield return new WaitForSeconds(5);
        
        LoadAllMessages();
    }

    private void SendChatMessage(string message) {
        if (message == string.Empty) {
            return;
        }
        StartCoroutine(SendMessageIEnumerator(message));
    }

    private IEnumerator SendMessageIEnumerator(string message) {
        Dictionary<string, string> headers = new Dictionary<string, string>();

        headers.Add("ContentType", "application/x-www-form-urlencoded");
        headers.Add("Authorization", "Token " + PlayerPrefs.GetString("app_token"));

        WWWForm form = new WWWForm();

        form.AddField("message", message);
        form.AddField("attach", "");
        form.AddField("created_at", DateTime.Now.ToString());
        form.AddField("answer", string.Empty);

        
        WWW www = new WWW("http://165.227.247.66/feedback/", form.data,headers);
        yield return www;
        input.text = string.Empty;
        if (www.isDone && string.IsNullOrEmpty(www.error)){
            StopCoroutine("LoadAllMessagesIEnumerator");
            LoadAllMessages();
        }else{
            MessageManager.instance.StartWaitWindow("Сообщение не доставлено!");
        }

    }

    private void ClousePanel() {
        this.gameObject.SetActive(false);
    }

    private void RemoveAll() {
        if (messagesObjects != null) {
            foreach (GameObject o in messagesObjects) {
                Destroy(o);
            }
        }
        messagesObjects = new List<GameObject>();
    }

    private void CreateMessagesObjs(string json) {
        if(json == null || json == string.Empty) return;
        Messages m = JsonUtility.FromJson<Messages>(json);

        if (m != null) {
                RemoveAll();
                
                foreach (MessageModel message in m.messages) {
                    if (message.message != String.Empty) {
                        GameObject obj = Helper.InstantiateUiObject(new Vector3(), incomingMessage, scrollRect.content);
                        obj.GetComponent<ChatMessageController>().SetDateLabel(
                                DateTime.Parse(message.created_at),
                                MainManager.instance.GetPersManager().GetPersValues().name
                            )
                            .SetMessage(message.message);
                        messagesObjects.Add(obj);
                    }
                    if (message.answer != String.Empty) {
                        GameObject obj = Helper.InstantiateUiObject(new Vector3(), outgoingMessage, scrollRect.content);
                        obj.GetComponent<ChatMessageController>().SetDateLabel(
                                DateTime.Parse(message.created_at ),
                                "Доктор"
                            )
                            .SetMessage(message.answer);
                        messagesObjects.Add(obj);
                    }

                }
            }
    }
}
