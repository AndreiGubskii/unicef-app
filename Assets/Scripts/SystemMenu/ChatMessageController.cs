﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChatMessageController : MonoBehaviour {

    [SerializeField] private Text dateLabel;
    [SerializeField] private Text messageText;

    public ChatMessageController SetDateLabel(DateTime date,string name=null) {
        dateLabel.text = string.Format("<color=#000000>{1}</color>   <color=#cccccc>{0}</color>",date.ToString("MM.dd.yyyy HH:mm"),name);
        return this;
    }

    public ChatMessageController SetMessage(string message) {
        messageText.text = message;
        return this;
    }
}
