﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;


public class SystemMenuManager : MonoBehaviour {
    [SerializeField] private Button faqBtn;
    [SerializeField] private Button messageBtn;
    [SerializeField] private Button callBtn;
    [SerializeField] private Button openSystemMenuBtn;
    [SerializeField] private Button clouseSystemMenuBtn;

    [SerializeField] private GameObject faqPanel;
    [SerializeField] private GameObject messagePanel;
    [SerializeField] private GameObject systemMenuPanel;
    [SerializeField] private NotificationLabel messageNotificationLabel;
    [SerializeField] private NotificationLabel systemMenuBtnNotificationLabel;
    private int newMessagesCount = 0;
    private int newMessagesCountPrev = 0;
    public int NewMessagesCount {
        get { return newMessagesCount; }
    }

    private string hashChat;

    public string HashChat {
        get { return hashChat; }
        set { hashChat = value; }
    }

    private string hashDoctorData;

    public string HashDoctorData
    {
        get { return hashDoctorData; }
        set { hashDoctorData = value; }
    }

    private string hashFAQData;
    public string HashFAQData {
        get { return hashFAQData; }
        set { hashFAQData = value; }
    }

    private string hashChatPath;
    private string hashDoctorDataPath;
    private string hashFAQDataPath;
    public static SystemMenuManager instance;
    public string HashChatPath {
        get { return hashChatPath; }
    }

    public string HashFAQDataPath {
        get { return hashFAQDataPath; }
    }

    private void Awake() {
        instance = this;
        hashChatPath = Application.persistentDataPath + "/hashChat.txt";
        hashDoctorDataPath = Application.persistentDataPath + "/hashDoctorData.txt";
        hashFAQDataPath = Application.persistentDataPath + "/hashFAQDataPath.txt";

        
    }

    private void Start() {
        openSystemMenuBtn.onClick.AddListener(OpenMenuListener);
        clouseSystemMenuBtn.onClick.AddListener(ClouseMenuListener);
        faqBtn.onClick.AddListener(FaqClickListener);
        messageBtn.onClick.AddListener(MessageClickListener);
        callBtn.onClick.AddListener(CallclickListener);

        if (!File.Exists(hashChatPath)){
            File.Create(hashChatPath);
        }else{
            HashChat = File.ReadAllText(hashChatPath);
        }
        if (!File.Exists(hashDoctorDataPath)){
            File.Create(hashDoctorDataPath);
        }else{
            HashDoctorData = File.ReadAllText(hashDoctorDataPath);
        }
        if (!File.Exists(hashFAQDataPath)){
            File.Create(hashFAQDataPath);
        }else{
            HashFAQData = File.ReadAllText(hashFAQDataPath);
        }
        LoadDoctorData();
        CheckNewMessage();
    }

    private void FixedUpdate() {
        //Оповщения на кнопках когда системное меню открыто
        if (systemMenuPanel.activeSelf) {
            if (!messageNotificationLabel.gameObject.activeSelf && NewMessagesCount > 0) {
                messageNotificationLabel.Label = NewMessagesCount.ToString();
                messageNotificationLabel.gameObject.SetActive(true);
            }
            else if (messageNotificationLabel.gameObject.activeSelf && NewMessagesCount <= 0){
                messageNotificationLabel.gameObject.SetActive(false);
            }
        }
        //Оповищение на кнопке "Системное меню", о событиях в Системной панели
        if (NewMessagesCount > 0) {
            if (!systemMenuBtnNotificationLabel.gameObject.activeSelf) {
                systemMenuBtnNotificationLabel.Label = NewMessagesCount.ToString();
                systemMenuBtnNotificationLabel.gameObject.SetActive(true);
            }
        }else if(systemMenuBtnNotificationLabel.gameObject.activeSelf) {
            systemMenuBtnNotificationLabel.gameObject.SetActive(false);
        }
        
    }

    private void OpenMenuListener() {
        if (systemMenuPanel != null) {
            systemMenuPanel.SetActive(true);
        }
    }

    private void ClouseMenuListener() {
        if (systemMenuPanel != null) {
            systemMenuPanel.SetActive(false);
        }
    }

    private void CallclickListener() {
        string number = "";
        try {
            number = GetDoctorData(HashDoctorData).phone_number;
        }catch (Exception e) {
            Debug.Log(e);
        }

        if (number != string.Empty) {
            Application.OpenURL("tel://" + number);
        }else {
            MessageManager.instance.StartWaitWindow("Не удалось загрузить данные...",LoadDoctorData);
        }
        
    }

    private void MessageClickListener() {
        if (messagePanel != null) {
            messagePanel.SetActive(true);
        }
    }

    private void FaqClickListener() {
        if (faqPanel != null) {
            faqPanel.SetActive(true);
        }
    }

    private void CheckNewMessage() {
        StartCoroutine(LoadAllMessagesIEnumerator());
    }

    private IEnumerator LoadAllMessagesIEnumerator(){
        while (messagePanel.activeSelf || !Helper.CheckConnectNetworking()) {
            yield return new WaitForEndOfFrame();
        }
        Dictionary<string, string> headers = new Dictionary<string, string>();

        headers.Add("ContentType", "application/x-www-form-urlencoded");
        headers.Add("Authorization", "Token " + PlayerPrefs.GetString("app_token"));

        WWW www = new WWW("http://165.227.247.66/feedback/", null, headers);
        yield return www;

        if (www.isDone && string.IsNullOrEmpty(www.error)){
            string json = "{\"messages\":" + www.text + "}";
            MessagePanelManager.Messages newMessages = JsonUtility.FromJson<MessagePanelManager.Messages>(json);
            MessagePanelManager.Messages oldMessages = JsonUtility.FromJson<MessagePanelManager.Messages>(HashChat);
            if (newMessages != null && oldMessages != null) {
                var n = from newM in newMessages.messages
                    where newM.answer != ""
                    select newM;

                var o = from newM in oldMessages.messages
                    where newM.answer != ""
                    select newM;

                newMessagesCount = n.Count() - o.Count();
            }

            if (newMessagesCount != 0 && newMessagesCountPrev != NewMessagesCount) {
                AudioManager.instance.PlaySound(
                    AudioSourcesEnums.PERS,
                    SoundEnums.NEWMESSAGE
                );
                newMessagesCountPrev = NewMessagesCount;
                //new mesage
            }
        }
        else{
        #if UNITY_EDITOR
            Debug.LogError(www.error);
        #endif

        }

        yield return new WaitForSeconds(5);

        CheckNewMessage();
    }


    [System.Serializable]
    public class DoctorDataModel{
        
            public int user;
            public int role;
            public DateTime birthdate;
            public string address;
            public string phone_number;
            public string code;
            public string creator;
        
    }

    [System.Serializable]
    public class DoctorData {
        public DoctorDataModel[] doctors;
    }

    private void LoadDoctorData() {
        StartCoroutine(LoadDoctorDataIEnumerator());
    }

    private IEnumerator LoadDoctorDataIEnumerator() {
        Dictionary<string, string> headers = new Dictionary<string, string>();

        headers.Add("ContentType", "application/x-www-form-urlencoded");
        headers.Add("Authorization", "Token " + PlayerPrefs.GetString("app_token"));

        WWW www = new WWW("http://165.227.247.66/doctor/", null, headers);
        yield return www;

        if (www.isDone &&  string.IsNullOrEmpty(www.error)){
            string json = "{\"doctors\":" + www.text + "}";
            if (www.text != string.Empty && json != HashDoctorData) {
                HashDoctorData = json;
                try {
                    File.WriteAllText(hashDoctorDataPath, HashDoctorData);
                }
                catch (Exception e) {
                    Debug.Log(e.Message);
                }
            }
        }
        else {
        #if UNITY_EDITOR
            Debug.LogError(www.error);
        #endif

        }
    }

    private DoctorDataModel GetDoctorData(string json) {
        DoctorData d = JsonUtility.FromJson<DoctorData>(json);

        return d.doctors.First();
    }
}
