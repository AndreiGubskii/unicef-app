﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NotificationLabel : MonoBehaviour {

    [SerializeField] private Text label;

    public string Label {
        set { label.text = value; }
    }
}
