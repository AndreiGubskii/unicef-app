﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Xml.Linq;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class FaqPanelManager : MonoBehaviour {
    [Serializable] public class FaqItemModel {
        public int id;
        public string question;
        public string answer;
    }
    [Serializable]
    public class FaqItems{
        public FaqItemModel[] items;
    }
    [SerializeField] private Button exitBtn;
    [SerializeField] private ScrollRect scrollRect;
    [SerializeField] private GameObject faqItemPrefab;
    private List<GameObject> itemsList = new List<GameObject>();
    public void Start(){
        exitBtn.onClick.AddListener(ClousePanel);
    }

    private void ClousePanel(){
        this.gameObject.SetActive(false);
    }

    private void OnEnable() {
        UploadFaq();
    }

    private void OnDisable() {
        DeleteAllItems();
    }

    private void UploadFaq(){
        StartCoroutine(UplloadFaqIenumerator(PlayerPrefs.GetString("app_token")));
    }
    private IEnumerator UplloadFaqIenumerator(string token)
    {
        LoaderUI.instance.ShowLoader();
        if (Helper.CheckConnectNetworking())
        {
            UnityWebRequest www = UnityWebRequest.Get("http://165.227.247.66/faq/");
            www.SetRequestHeader("Authorization", "Token " + token);

            yield return www.Send();
            if (www.isNetworkError){
            #if UNITY_EDITOR
                Debug.Log(www.error);
            #endif
            }
            else {
                string json = "{\"items\": " + www.downloadHandler.text + "}";
                if (SystemMenuManager.instance.HashFAQData != json) {
                    File.WriteAllText(SystemMenuManager.instance.HashFAQDataPath, json);
                }
                CreateFaqItems(json);
            }
        }else{
            MessageManager.instance.StartWaitWindow("Нет подключения!", LoadLocalFaq);
        }
        LoaderUI.instance.HideLoaderPanel();
    }

    private void CreateFaqItems(string json) {
        FaqItemModel[] items = JsonUtility.FromJson<FaqItems>(json).items;

        DeleteAllItems();
        foreach (FaqItemModel item in items) {
            GameObject obj = Instantiate(faqItemPrefab, scrollRect.content);
            FaqItem faqItem = obj.GetComponent<FaqItem>();
            faqItem.Id = item.id;
            string txt = "<size=18>" + item.question + "</size>\n" + "<size=14><color=#727272FF>" + item.answer+ "</color></size>";
            faqItem.FAQText = txt;
            itemsList.Add(obj);
        }
    }

    private void DeleteAllItems() {
        if (itemsList != null) {
            foreach (GameObject o in itemsList) {
                Destroy(o);
            }
        }
        itemsList = new List<GameObject>();
    }

    private void LoadLocalFaq() {
        if (SystemMenuManager.instance.HashFAQData != string.Empty) {
            CreateFaqItems(SystemMenuManager.instance.HashFAQData);
        }
    }
}
