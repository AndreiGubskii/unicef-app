﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FaqItem : MonoBehaviour {
    
    public int Id { get; set; }

    public string FAQText {
        get { return text.text; }
        set {
            text.text = value;

            
        }
    }

    [SerializeField] private Text text;

}
