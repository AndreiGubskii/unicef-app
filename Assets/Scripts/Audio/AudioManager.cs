﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class AudioManager : MonoBehaviour {
    [System.Serializable]
    public class _Sound {
        public SoundEnums name;
        public string audio;
    }

    [System.Serializable]
    public class _AudioSource{
        public AudioSourcesEnums name;
        public AudioSource audioSource;
    }

    [SerializeField] private _Sound[] sounds;
    [SerializeField] private _AudioSource[] audioSources;

    public static AudioManager instance;

    private void Awake() {
        //DontDestroyOnLoad(this);
        instance = this;
    }

    public _AudioSource GetAudioSource(AudioSourcesEnums type) {
        return this.audioSources.First(m => m.name == type);
    }

    public AudioClip GetAudioClip(SoundEnums name) {
        return Resources.Load<AudioClip>("Sounds/"+this.sounds.First(m => m.name == name).audio);
    }

    public void PlaySoundOneShot(AudioSourcesEnums type, SoundEnums name) {
        _AudioSource source = GetAudioSource(type);
        AudioClip clip = GetAudioClip(name);
        if (source.audioSource != null) {
            source.audioSource.PlayOneShot(clip);
        }
        Resources.UnloadUnusedAssets();
    }

    public void PlaySound(AudioSourcesEnums type, SoundEnums name) {
        _AudioSource source = GetAudioSource(type);
        AudioClip clip = GetAudioClip(name);
        if (source.audioSource != null) {
            source.audioSource.clip = clip;
            source.audioSource.Play();
        }
        Resources.UnloadUnusedAssets();
    }

    public void PlaySound(AudioSourcesEnums type, SoundEnums name,ulong delay) {
        _AudioSource source = GetAudioSource(type);
        AudioClip clip = GetAudioClip(name);
        if (source.audioSource != null) {
            source.audioSource.clip = clip;
            source.audioSource.PlayDelayed(delay);
        }
        Resources.UnloadUnusedAssets();
    }
}
