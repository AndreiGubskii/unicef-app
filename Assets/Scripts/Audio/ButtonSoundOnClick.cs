﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ButtonSoundOnClick : MonoBehaviour,IPointerClickHandler {

    public void OnPointerClick(PointerEventData eventData) {
        AudioManager.instance.PlaySoundOneShot(
            AudioSourcesEnums.BUTTONS,
            SoundEnums.BUTTON
        );
    }
}
