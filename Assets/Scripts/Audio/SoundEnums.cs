﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum SoundEnums {
    DEFAULT,
    BUTTON,
    LEVELUP,
    EATING,
    PLAYING,
    CARESS,
    NEWMESSAGE,
    GETCOIN,
    SATIETY,
    HAPPINESS,
    PURCHASE,
    CARESS_DOG

}
