﻿ using System;
 using System.Collections;
using System.Collections.Generic;
 using System.Globalization;
 using System.Net;
 using System.Text;
 using UnityEngine;

public class SenderDataManager : MonoBehaviour {
    [System.Serializable] public class ResponseStatus {
        public string status;
    }
    public static SenderDataManager instance;
    private List<DB_Tables.TasksTable> taskList;
    private void Awake() {
        instance = this;
    }

    public void SendData() {
        taskList = DB_Query.instance.SelectTaskBySentStatus(0);
        if (taskList.Count > 0 && Helper.CheckConnectNetworking()) {
            string query = "";
            foreach (DB_Tables.TasksTable task in taskList) {
                string time = "";
                string status = "";
                if (task.status == Tasks.TskStatus.COMPLATE) {
                    status = "1";
                    time = task.lead_time.ToString("G", CultureInfo.GetCultureInfo("ru-RU"));
                }else if(task.status == Tasks.TskStatus.TOOK_AWAY_HEALTH) {
                    status = "0";
                    time = "";
                }
                else {
                    continue;
                }
                if (taskList.IndexOf(task) == taskList.Count-1) {
                    query += string.Format("\"{0}\": {{\"take\":{1},\"time\":\"{2}\"}}", task.drug_id, status,time);
                } else {
                    query += string.Format("\"{0}\": {{\"take\":{1},\"time\":\"{2}\"}},", task.drug_id, status,time);
                }
            }
            StartCoroutine(UploadIEnumerator(query));
        }
    }

     IEnumerator UploadIEnumerator(string data){
        yield return new WaitForSeconds(1f);
        using (var client = new WebClient()) {
           string json = "{"+data+"}";
            client.Headers[HttpRequestHeader.ContentType] = "json";
            client.Headers[HttpRequestHeader.Authorization] = "Token " + PlayerPrefs.GetString("app_token");
            json = Encoding.Default.GetString(Encoding.UTF8.GetBytes(json));
            var result = client.UploadString("http://165.227.247.66/report/", "POST", json);
            string status = JsonUtility.FromJson<ResponseStatus>(result).status;
            if (status == "0") {
                foreach (DB_Tables.TasksTable tasks in taskList) {
                    DB_Query.instance.SetSended(tasks.id);
                }
                //MessageManager.instance.StartWaitWindow("Данные успешно переданны на сервер! " + json);
            }
            else {
                //MessageManager.instance.StartWaitWindow("Не удалось отправить данные!");
            }
        }
    }
}
