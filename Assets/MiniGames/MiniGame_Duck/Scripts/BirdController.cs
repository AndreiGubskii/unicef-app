﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.EventSystems;

public class BirdController : MonoBehaviour,IPointerDownHandler {
    public delegate void BirdControllerDelegate(GameObject obj);
    public event BirdControllerDelegate BirdDeadEvent;
    [SerializeField] private BirdAnimatorController animatorController;
    private float dynamicSpeed = 0;

    private Vector3 direction;

    private void Start() {
        this.direction = Direction();
    }

    void Update(){
        if(Duck_MainManager.instance.Pause) return;
        if (dynamicSpeed != Duck_MainManager.instance.DuckSpeed){
            dynamicSpeed = Duck_MainManager.instance.DuckSpeed;
        }
        if (transform.localPosition == this.direction){
            this.direction = this.Direction();
        }
        if (this.direction.x < transform.localPosition.x && this.transform.localScale.x > -1f)
        {
            this.transform.localScale = new Vector3(-1f, 1f, 1f);
        }
        else if (this.direction.x > transform.localPosition.x && this.transform.localScale.x < 1f)
        {
            this.transform.localScale = new Vector3(1f, 1f, 1f);
        }
        transform.localPosition = Vector3.MoveTowards(transform.localPosition, this.direction, dynamicSpeed * Time.deltaTime);
    }

    public void OnPointerDown(PointerEventData eventData) {
        Dead();
    }

    public void Dead() {
        if (BirdDeadEvent != null) {
            BirdDeadEvent(this.gameObject);
        }
        animatorController.PalayDead();
        direction = new Vector3(transform.localPosition.x,-500f);
        dynamicSpeed = 500f;
        Destroy(this.gameObject, 0.5f);
    }

    private Vector3 Direction(){
        float x = Random.Range(-110, 110);
        float y = Random.Range(-250, 250);
        float z = 0f;
        return new Vector3(x, y, z);
    }
}
