﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BirdAnimatorController : MonoBehaviour {
    private Animator _animator;

    private void Awake() {
        _animator = GetComponent<Animator>();
    }

    public void PalayDead() {
        _animator.SetTrigger("dead");
    }
}
