﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Duck_BackGround : MonoBehaviour,IPointerDownHandler{
    public delegate void Duck_BackGroundDelegate();

    public event Duck_BackGroundDelegate TapOnBackEvents;

    public void OnPointerDown(PointerEventData eventData) {
        if (TapOnBackEvents != null) {
            TapOnBackEvents();
        }
    }
}
