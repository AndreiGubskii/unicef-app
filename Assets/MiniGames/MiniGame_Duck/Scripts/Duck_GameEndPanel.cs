﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Duck_GameEndPanel : MonoBehaviour {
    [SerializeField] private GameObject gameOverLabel;
    [SerializeField] private GameObject winLabel;
    [SerializeField] private GameObject pauseLabel;

    [SerializeField] private Button restartBtn;
    [SerializeField] private Button cancelBtn;
    [SerializeField] private Button exitBtn;

    public WinAnimate WinAnimatePanel {
        get { return winLabel.GetComponent<WinAnimate>(); }
    }

    private void OnEnable() {
        restartBtn.onClick.AddListener(RestartListener);
        cancelBtn.onClick.AddListener(CancelListener);
        exitBtn.onClick.AddListener(ExitListener);
        if (!Duck_MainManager.instance.Game) {
            if (Duck_MainManager.instance.Life <= 0) {
                gameOverLabel.SetActive(true);
                winLabel.SetActive(false);
                pauseLabel.SetActive(false);
                exitBtn.gameObject.SetActive(false);
            }
            else {
                gameOverLabel.SetActive(false);
                winLabel.SetActive(true);
                pauseLabel.SetActive(false);
                exitBtn.gameObject.SetActive(false);

                DB_Query.instance.AddHappiness(0.1f);
                WinAnimatePanel.StartAnimation(Duck_MainManager.instance.DeadBirdsCounter,10);
            }
        }
        else if(Duck_MainManager.instance.Pause) {
            gameOverLabel.SetActive(false);
            winLabel.SetActive(false);
            pauseLabel.SetActive(true);
            exitBtn.gameObject.SetActive(true);
        }
    }

    private void ExitListener() {
        MessageManager.instance.StartConfirmWindow("Выйти из игры?",LoadMainScene);
    }

    private void LoadMainScene() {
        LoadingManager.instance.LoadScene(2);
    }

    private void CancelListener() {
        if (!Duck_MainManager.instance.Game) {
            LoadMainScene();
        }
        else if(Duck_MainManager.instance.Pause) {
            Duck_MainManager.instance.ResumeGame();
        }
    }

    private void RestartListener() {
        Duck_MainManager.instance.Restart();
    }

    private void OnDisable() {
        restartBtn.onClick.RemoveListener(RestartListener);
        cancelBtn.onClick.RemoveListener(CancelListener);
        exitBtn.onClick.RemoveListener(ExitListener);
    }
}
