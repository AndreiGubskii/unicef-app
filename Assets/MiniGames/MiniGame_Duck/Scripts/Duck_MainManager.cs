﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Duck_MainManager : MonoBehaviour {

    private int life = 3;

    private float timerLevel;
    private float timerLevelValue;
    private bool game;
    private bool pause;
    private List<GameObject> birds;
    private float delayBeforeInst = 0f;
    
    public float DuckSpeed {
        get { return _duckSpeed; }
        set { _duckSpeed = value; }
    }
    private float _duckSpeed;

    public bool Game {
        get { return game; }
    }

    public bool Pause {
        get { return pause; }
    }

    public int Life {
        get { return life; }
    }
    [SerializeField] private TimeManager timer;
    [SerializeField] private GameObject birdPrefab;
    [SerializeField] private Transform[] birdsSpawn;
    [SerializeField] private Transform birdsPanel;
    [SerializeField] private Text timerLabel;
    [SerializeField] private GameObject GameEndPanel;
    [SerializeField] private Button pauseBtn;
    [SerializeField] private Duck_BackGround backGround;
    [SerializeField] private Duck_LifeUiManager lifeUiManager;
    [SerializeField] private AudioSource soundAudioSource;
    public static Duck_MainManager instance;
    private int deadBirdsCounter;

    public int DeadBirdsCounter {
        get {
            return deadBirdsCounter;
        }
    }

    private void Awake() {
        instance = this;
        pauseBtn.onClick.AddListener(PauseListener);
        backGround.TapOnBackEvents += СatcherOfFailure;

        timer.StopTimerEvent += StopTimerListener;
    }

    private void StopTimerListener() {
        game = false;
    }

    private void СatcherOfFailure() {
        if (Life > 0) {
            life--;
            lifeUiManager.TakeAwayHeart();

            if (soundAudioSource.clip == null || soundAudioSource.clip.name != "fail"){
                soundAudioSource.clip = Resources.Load<AudioClip>("DuckSound/fail");
            }

            soundAudioSource.Play();
        }
        
    }

    private void PauseListener() {
        pause = true;
    }

    private void Start() {
        Restart();
    }

    private void Update() {
        if (Input.GetKeyUp(KeyCode.Escape)) {
            PauseListener();
        }
        if (Game && !Pause) {
            if (delayBeforeInst > 0 && birds.Count < 5) {
                delayBeforeInst -= 1 * Time.deltaTime;
            }else if(birds.Count < 5)  {
                InstBird();
                delayBeforeInst = 2;
            }

            if (Life <= 0) {
                game = false;
            }

            if (timerLevel > 0) {
                timerLevel -= 1 * Time.deltaTime;
            }
            else {
                timerLevel = timerLevelValue;
                DuckSpeed += 50;
            }
        }
        if ((!Game || Pause) && !GameEndPanel.activeSelf) {
            GameEndPanel.SetActive(true);
            timer.PauseTimer();
        }
    }

    public void Restart() {
        DestroyAllBirds();
        lifeUiManager.UpdateHearts();
        pause = false;
        game = true;
        life = 3;
        timer.RestartTimer();
        GameEndPanel.SetActive(false);
        DuckSpeed = 200f;
        timerLevelValue = timer.Timer * 30 / 100;
        timerLevel = timerLevelValue;
        deadBirdsCounter = 0;
    }

    public void ResumeGame() {
        pause = false;
        GameEndPanel.SetActive(false);
        timer.ResumeTimer();
    }

    private void DestroyAllBirds() {
        if (birds != null) {
            foreach (GameObject o in birds) {
                Destroy(o);
            }
        }
        birds = new List<GameObject>();
    }

    private void InstBird() {
        GameObject obj = Instantiate(birdPrefab, birdsSpawn[Random.Range(0, birdsSpawn.Length)].position,
            Quaternion.identity, birdsPanel);
        obj.GetComponent<BirdController>().BirdDeadEvent += BirdDeadEventListener;
        birds.Add(obj);
    }

    private void BirdDeadEventListener(GameObject obj) {
        birds.Remove(obj);
        deadBirdsCounter++;
        if (soundAudioSource.clip == null || soundAudioSource.clip.name != "dead") {
            soundAudioSource.clip = Resources.Load<AudioClip>("DuckSound/dead");
        }
        
        soundAudioSource.Play();
    }

}
