﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WinAnimate : MonoBehaviour {

    public GameObject winLabel;
    public Text counter;
    public Text coinLabel;
    private Animator _animator;

    private void OnEnable() {
        coinLabel.text = GetCoins().ToString();
    }

    private Animator GetAnimator() {
        return _animator == null ? _animator = GetComponent<Animator>() : _animator;
    }

    public void StartAnimation(int value, int top) {
        StartCoroutine(CounterIEnumerator(value, top));
    }

    private IEnumerator CounterIEnumerator(int value,int top) {
        for (int i = 0; i < value; i++) {
            yield return new WaitForSeconds(0.05f);
            counter.text = i.ToString();
            if (i == top) {
                top += top;
                GetAnimator().SetTrigger("winAnimation_coin");
                yield return new WaitForSeconds(0.5f);
            }
        }
    }

    public void WinAnimation_coin_EndListener() {
        AddCoins(1);
    }

    public int GetCoins(){
        int coins = 0;
        int.TryParse(DB_Query.instance.GetCoins(), out coins);
        return coins;
    }
    private void AddCoins(int value){
        DB_Query.instance.AddCoin(value, UpdateCoins);
    }


    private void UpdateCoins(){
        coinLabel.text = GetCoins().ToString();
    }
}
