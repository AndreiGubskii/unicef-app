﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class FingerCatcher : MonoBehaviour,IPointerDownHandler,IPointerUpHandler{

    public delegate void FingerCatcherDelegate();

    public event FingerCatcherDelegate OnPointerDownEvent;
    public event FingerCatcherDelegate OnPointerUpEvent;
    public void OnPointerDown(PointerEventData eventData) {
        if (OnPointerDownEvent != null) {
            OnPointerDownEvent();
        }
    }

    public void OnPointerUp(PointerEventData eventData) {
        if (OnPointerUpEvent != null) {
            OnPointerUpEvent();
        }
    }
}
