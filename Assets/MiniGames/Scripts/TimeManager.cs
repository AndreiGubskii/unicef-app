﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TimeManager : MonoBehaviour
{
    public delegate void TimeManagerDelegate();

    public event TimeManagerDelegate StartTimerEvent;
    public event TimeManagerDelegate StopTimerEvent;
    public event TimeManagerDelegate PauseTimerEvent;
    public event TimeManagerDelegate ResumeTimerEvent;

    private float _timer;
    private bool isPlay;

    public float Timer {
        get { return timer; }
    }
    [SerializeField] private AudioSource _audioSource;
    [SerializeField] private Animator _animator;
    [SerializeField] private float timer;
    [SerializeField] private Text timerLabel;

    void Start(){

    }

    void Update(){
        if (isPlay) {
            if (_timer >= 0){
                if (!_audioSource.isPlaying && _timer <= 5) {
                    _audioSource.Play();
                    _animator.SetTrigger("overTime");
                }
                _timer -= (1f * Time.deltaTime);
                timerLabel.text = _timer.ToString("0:00");
            }
            else {
                isPlay = false;
                _audioSource.Stop();
                if (StopTimerEvent != null){
                    StopTimerEvent();
                }
            }
        }

    }

    public void StartTimer() {
        if (_audioSource.isPlaying) {
            _audioSource.Stop();
        }
        
        _timer = timer;
        isPlay = true;
        if (StartTimerEvent != null) {
            StartTimerEvent();
        }
    }

    public void PauseTimer() {
        if (_audioSource.isPlaying) {
            _audioSource.Stop();
        }
        isPlay = false;
        if (PauseTimerEvent != null){
            PauseTimerEvent();
        }
    }

    public void ResumeTimer() {
        isPlay = true;
        if (ResumeTimerEvent != null){
            ResumeTimerEvent();
        }
    }

    public void RestartTimer() {
        StartTimer();
    }

    

}
