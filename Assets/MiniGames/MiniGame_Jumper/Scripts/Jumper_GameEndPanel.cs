﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Jumper_GameEndPanel : MonoBehaviour {
    [SerializeField] private GameObject gameOverLabel;
    [SerializeField] private GameObject winLabel;
    [SerializeField] private GameObject pauseLabel;

    [SerializeField] private Button restartBtn;
    [SerializeField] private Button cancelBtn;
    [SerializeField] private Button exitBtn;

    public WinAnimate WinAnimatePanel{
        get { return winLabel.GetComponent<WinAnimate>(); }
    }
    private void OnEnable() {
        restartBtn.onClick.AddListener(RestartListener);
        cancelBtn.onClick.AddListener(CancelListener);
        exitBtn.onClick.AddListener(ExitListener);
        if (!Jumper_MainManager.instance.Game) {
            if (Jumper_MainManager.instance.JumperIsDead()) {
                gameOverLabel.SetActive(true);
                winLabel.SetActive(false);
                pauseLabel.SetActive(false);

                exitBtn.gameObject.SetActive(false);
            }
            else {
                gameOverLabel.SetActive(false);
                winLabel.SetActive(true);
                pauseLabel.SetActive(false);

                exitBtn.gameObject.SetActive(false);

                WinAnimatePanel.StartAnimation(Jumper_MainManager.instance.MaxPlayerPosition, 50);

                DB_Query.instance.AddHappiness(0.1f);
            }
        }
        else if(Jumper_MainManager.instance.Pause) {
            gameOverLabel.SetActive(false);
            winLabel.SetActive(false);
            pauseLabel.SetActive(true);
            exitBtn.gameObject.SetActive(true);
        }
    }

    private void ExitListener() {
        MessageManager.instance.StartConfirmWindow("Выйти из игры?",LoadMainScene);
    }

    private void LoadMainScene() {
        LoadingManager.instance.LoadScene(2);
    }

    private void CancelListener() {
        if (!Jumper_MainManager.instance.Game) {
            LoadMainScene();
        }
        else if(Jumper_MainManager.instance.Pause) {
            Jumper_MainManager.instance.ResumeGame();
        }
    }

    private void RestartListener() {
        Jumper_MainManager.instance.Restart();
    }

    private void OnDisable() {
        restartBtn.onClick.RemoveListener(RestartListener);
        cancelBtn.onClick.RemoveListener(CancelListener);
        exitBtn.onClick.RemoveListener(ExitListener);
    }
}
