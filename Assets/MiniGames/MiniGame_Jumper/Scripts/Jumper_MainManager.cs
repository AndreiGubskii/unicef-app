﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Jumper_MainManager : MonoBehaviour {

    private bool game;
    private bool pause;

    public bool Game {
        get { return game; }
    }

    public bool Pause {
        get { return pause; }
    }

    public AudioSource PlatformSound {
        get {
            return platformSound;
        }
    }
    [SerializeField] private AudioSource platformSound;
    [SerializeField] private TimeManager timer;
    [SerializeField] private GameObject GameEndPanel;
    [SerializeField] private GameObject player;
    [SerializeField] private Button pauseBtn;

    public static Jumper_MainManager instance;
    private float playerPosition;

    public int MaxPlayerPosition {
        get { return (int) playerPosition; }
    }

    private void Awake() {
        instance = this;
        pauseBtn.onClick.AddListener(PauseListener);
        player.GetComponent<Jumper_Player>().Dead += JumperDeadListener;

        timer.StopTimerEvent += StopTimerListener;
    }

    private void StopTimerListener() {
        game = false;
    }

    private void JumperDeadListener() {
        game = false;
    }

    private void PauseListener() {
        pause = true;
    }

    private void Start() {
        pause = false;
        game = true;
        timer.RestartTimer();
        GameEndPanel.SetActive(false);
    }

    private void Update() {
        if (Input.GetKeyUp(KeyCode.Escape)) {
            PauseListener();
        }
        if (Game && !Pause) {
            playerPosition = player.transform.localPosition.y;
        }
        if ((!Game || Pause) && !GameEndPanel.activeSelf) {
            GameEndPanel.SetActive(true);
            timer.PauseTimer();
        }
        
    }

    public void Restart() {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void ResumeGame() {
        pause = false;
        GameEndPanel.SetActive(false);
        timer.ResumeTimer();
    }

    public bool JumperIsDead() {
        if (player == null) {
            return true;
        }
        return false;
    }
}
