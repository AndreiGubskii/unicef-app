using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class Jumper_Player : MonoBehaviour {

    public delegate void Jumper_PlayerDelegate();

    public event Jumper_PlayerDelegate Dead;

    public float movementSpeed = 10f;
    [SerializeField] private AudioSource _audioSource;
	Rigidbody2D rb;

	float movement = 0f;
    private float maxPosition = 0f;
	void Start () {
		rb = GetComponent<Rigidbody2D>();
	}
	
	void Update () {
	    if (Jumper_MainManager.instance.Pause || !Jumper_MainManager.instance.Game) return;
#if UNITY_EDITOR_WIN || UNITY_EDITOR_OSX || UNITY_STANDALONE_OSX || UNITY_STANDALONE_WIN
        movement = Input.GetAxis("Horizontal") * movementSpeed * Time.deltaTime;
#elif UNITY_ANDROID
    movement = Input.acceleration.x * movementSpeed * Time.deltaTime;
#endif

    }

    void FixedUpdate(){
	    if (Jumper_MainManager.instance.Pause || !Jumper_MainManager.instance.Game) {
	        if (rb.simulated) {
	            rb.simulated = false;
            }
            return;
	    }
	    if (!rb.simulated) {
	        rb.simulated = true;
	    }


	    Vector2 velocity = rb.velocity;
		velocity.x = movement;
		rb.velocity = velocity;

	    if (transform.localPosition.x > 3.5) {
	        transform.localPosition = new Vector3(-3.5f, transform.localPosition.y);
        }
        if (transform.localPosition.x < -3.5) {
	        transform.localPosition = new Vector3(3.5f, transform.localPosition.y);
        }
	    if (transform.localPosition.y > maxPosition) {
	        maxPosition = transform.localPosition.y;
	    }

        if (!_audioSource.isPlaying && transform.localPosition.y < (maxPosition - 5f)) {
            _audioSource.Play();
        }
        if (transform.localPosition.y < (maxPosition - 30f)) {
	        Destroy(gameObject);
	    }
	}

    private void OnDestroy() {
        if (Dead != null) {
            Dead();
        }
    }
}
