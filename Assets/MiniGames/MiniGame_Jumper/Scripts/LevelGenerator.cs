using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelGenerator : MonoBehaviour {

	public GameObject[] platformPrefabs;

	public int numberOfPlatforms = 200;
	public float levelWidth = 1.5f;
	public float minY = .2f;
	public float maxY = 1f;

	// Use this for initialization
	void Start () {

		Vector3 spawnPosition = new Vector3(0,-1.5f,0);

		for (int i = 0; i < numberOfPlatforms; i++)
		{
			spawnPosition.y += Random.Range(minY, maxY);
			spawnPosition.x = Random.Range(-levelWidth, levelWidth);
			Instantiate(platformPrefabs[Random.Range(0,platformPrefabs.Length)], spawnPosition, Quaternion.identity);
		}
	}
}
