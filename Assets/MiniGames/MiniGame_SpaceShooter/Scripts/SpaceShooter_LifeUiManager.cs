﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class SpaceShooter_LifeUiManager : MonoBehaviour {

    [SerializeField] private GameObject heartPrefab;
    private List<GameObject> hearts;

    private void Awake() {
        CreateHearts(SpaceShooter_MainManager.instance.Life);
    }

    private void Update() {
        
    }

    private void CreateHearts(int count) {
        hearts = new List<GameObject>();
        for (int i = 0; i < count; i++) {
            GameObject obj = Instantiate(heartPrefab, this.transform);
            hearts.Add(obj);
        }
    }

    public void TakeAwayHeart() {
        hearts.Last(m=>m.activeSelf).SetActive(false);
    }
    public void AddHeart(){
        hearts.First(m => !m.activeSelf).SetActive(true);
    }

    public void UpdateHearts() {
        GameObject[] objs = hearts.Where(m => !m.activeSelf).ToArray();
        foreach (GameObject o in objs) {
            o.SetActive(true);
        }
    }
}
