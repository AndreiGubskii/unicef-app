﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PersRocketController : MonoBehaviour {
    public delegate void PersRocketControllerDelegate();

    public event PersRocketControllerDelegate PersRocketTakeDamageEvents;
 
    [SerializeField] private GameObject bullet;
    [SerializeField] private Transform bulletSpawn;
    private bool atack;
	
	void Update () {
        if(!SpaceShooter_MainManager.instance.Game || SpaceShooter_MainManager.instance.Pause) return;
#if UNITY_EDITOR_WIN || UNITY_EDITOR_OSX || UNITY_STANDALONE_OSX || UNITY_STANDALONE_WIN
        if (Input.GetKey(KeyCode.RightArrow)) {
            RightMove();
        }
        else if (Input.GetKey(KeyCode.LeftArrow)) {
	        LeftMove();
        }
#elif UNITY_ANDROID
        if(Input.acceleration.x > 0){
            RightMove();
        }else if(Input.acceleration.x < 0){
            LeftMove();
        }
#endif
    }

    private void Attack() {
        if (!atack) {
            atack = true;
            StartCoroutine(AttackIEnumerator());
        }
        
    }

    private IEnumerator AttackIEnumerator() {
        while (!SpaceShooter_MainManager.instance.Game || SpaceShooter_MainManager.instance.Pause){
            yield return new WaitForEndOfFrame();
        }
        for (int i = 0; i < 10; i++) {
            if (SpaceShooter_MainManager.instance.Game && !SpaceShooter_MainManager.instance.Pause) {
                Instantiate(bullet, bulletSpawn.position, Quaternion.identity);
            }
            
            yield return new WaitForSeconds(0.3f);
        }
        StartCoroutine(AttackIEnumerator());
    }

    private void OnTriggerEnter(Collider col) {
        if (col.gameObject.GetComponent<EnemyBulletController>()!=null) {
            if (null != PersRocketTakeDamageEvents) {
                PersRocketTakeDamageEvents();
            }
            //Destroy(col.gameObject);
        }
    }

    public void LeftMove() {
        if (transform.localPosition.x > -3) {
            transform.localPosition = new Vector3(transform.localPosition.x - 4 * Time.deltaTime, transform.localPosition.y, 0);
        }
        Attack();
    }

    public void RightMove() {
        if (transform.localPosition.x < 3) {
            transform.localPosition = new Vector3(transform.localPosition.x + 4 * Time.deltaTime, transform.localPosition.y, 0);
        }
        Attack();
    }
}
