﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour {
    public delegate void EnemyControllerDelegate(GameObject obj);

    public event EnemyControllerDelegate DeadEnemyEvent;
    public float speed;

    public float delayBeforeShot;
    public float delayBeforeBullet;
    public int bulletCount;
    [SerializeField] private GameObject bullet;
    [SerializeField] private GameObject explosion;
    [SerializeField] private Transform bulletSpawn;
    private bool move;

    private Vector3 target;

    // Use this for initialization
	void Start () {
	    target = new Vector3(transform.localPosition.x,-4.5f,0);
        move = true;
        Attack();
	}
	
	// Update is called once per frame
	void Update () {
	    if (move && SpaceShooter_MainManager.instance.Game && !SpaceShooter_MainManager.instance.Pause) {
	        transform.localPosition = Vector3.MoveTowards(transform.localPosition, target, speed * Time.deltaTime);
	        if (transform.localPosition == target) {
	            SpaceShooter_MainManager.instance.PersRocketTakeDamageListener();
	            Destroy(this.gameObject);
            }
	    }
	}

    private void Attack() {
        StartCoroutine(AttackIEnumerator());
    }

    private IEnumerator AttackIEnumerator() {
        while (!SpaceShooter_MainManager.instance.Game || SpaceShooter_MainManager.instance.Pause) {
            yield return new WaitForEndOfFrame();
        }
        yield return new WaitForSeconds(delayBeforeShot);
        for (int i = 0; i < bulletCount; i++) {
            Instantiate(bullet, bulletSpawn.position, Quaternion.identity);
            yield return new WaitForSeconds(delayBeforeBullet);
        }
        StartCoroutine(AttackIEnumerator());
    }

    private void OnTriggerEnter(Collider col){
        if (col.gameObject.GetComponent<PersBulletController>() != null){
            Destroy(this.gameObject);
            Destroy(col.gameObject);
            GameObject obj = Instantiate(explosion, this.transform.localPosition, Quaternion.identity);
            Destroy(obj, 1f);
        }
    }

    private void OnDestroy() {
        if (DeadEnemyEvent != null){
            DeadEnemyEvent(this.gameObject);
        }
    }

}
