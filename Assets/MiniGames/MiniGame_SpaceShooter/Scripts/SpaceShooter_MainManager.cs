﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpaceShooter_MainManager : MonoBehaviour {

	private int life = 3;
    private bool game;
    private bool pause;
    private List<GameObject> enemysList;
    private float delayBeforeInst = 0f;
    public bool Game {
        get { return game; }
    }

    public bool Pause {
        get { return pause; }
    }

    public int Life {
        get { return life; }
    }

    [SerializeField] private TimeManager timer;
    [SerializeField] private GameObject[] enemys;
    [SerializeField] private Transform[] enemysSpawn;

    [SerializeField] private GameObject GameEndPanel;
    [SerializeField] private Button pauseBtn;

    [SerializeField] private SpaceShooter_LifeUiManager lifeUiManager;
    [SerializeField] private PersRocketController persRocket;

    [SerializeField] private FingerCatcher rigthMoveBtn;
    [SerializeField] private FingerCatcher leftMoveBtn;
    public static SpaceShooter_MainManager instance;
    private bool left, rigth;
    private int deadEnemyCounter;

    public int DeadEnemyCounter {
        get { return deadEnemyCounter; }
    }

    private void Awake() {
        instance = this;
        pauseBtn.onClick.AddListener(PauseListener);
        persRocket.PersRocketTakeDamageEvents += PersRocketTakeDamageListener;
        rigthMoveBtn.OnPointerDownEvent += RigthDownListener;
        leftMoveBtn.OnPointerDownEvent += LeftDownListener;

        rigthMoveBtn.OnPointerUpEvent += RigthUpListener;
        leftMoveBtn.OnPointerUpEvent += LeftUpListener;

        timer.StopTimerEvent += StopTimerListener;


    }


    private void LeftUpListener() {
        left = false;
    }

    private void RigthUpListener() {
        rigth = false;
    }

    private void LeftDownListener() {
        left = true;
    }

    private void RigthDownListener() {
        rigth = true;
    }

    private void PauseListener() {
        pause = true;
    }

    private void Start() {
        Restart();
    }

    private void Update() {
        if (Input.GetKeyUp(KeyCode.Escape)){
            PauseListener();
        }
        if (Game && !Pause) {
            
            if (delayBeforeInst > 0 && enemysList.Count < 5) {
                delayBeforeInst -= 1 * Time.deltaTime;
            }else if(enemysList.Count < 5)  {
                InstEnemy();
                delayBeforeInst = 2;
            }

            if (Life <= 0) {
                game = false;
            }
        }
        if ((!Game || Pause) && !GameEndPanel.activeSelf) {
            GameEndPanel.SetActive(true);
            timer.PauseTimer();
        }

        if (left) {
            persRocket.LeftMove();
        }

        if (rigth) {
            persRocket.RightMove();
        }
    }

    public void Restart() {
        DestroyAllEnemy();
        lifeUiManager.UpdateHearts();
        pause = false;
        game = true;
        life = 3;
        timer.RestartTimer();
        GameEndPanel.SetActive(false);
        DestroyAllBullets();
        deadEnemyCounter = 0;
    }

    public void ResumeGame() {
        pause = false;
        GameEndPanel.SetActive(false);
        timer.ResumeTimer();
    }

    private void DestroyAllEnemy() {
        if (enemysList != null) {
            foreach (GameObject o in enemysList) {
                Destroy(o);
            }
        }
        enemysList = new List<GameObject>();
    }

    private Vector3 prevPosition = new Vector3();
    private void InstEnemy() {
        Vector3 pos = enemysSpawn[Random.Range(0, enemysSpawn.Length)].position;
        while (prevPosition == pos) {
            pos = enemysSpawn[Random.Range(0, enemysSpawn.Length)].position;
        }
        prevPosition = pos;
        GameObject obj = Instantiate(enemys[Random.Range(0,enemys.Length)], pos,
            Quaternion.identity);
        obj.GetComponent<EnemyController>().DeadEnemyEvent += EnemyDeadEventListener;
        enemysList.Add(obj);
    }

    private void EnemyDeadEventListener(GameObject obj) {
        enemysList.Remove(obj);
        deadEnemyCounter++;
    }

    public void PersRocketTakeDamageListener() {
        life--;
        lifeUiManager.TakeAwayHeart();
    }

    private void DestroyAllBullets() {
        EnemyBulletController[] enemyBullets = FindObjectsOfType<EnemyBulletController>();
        PersBulletController[] persBullets = FindObjectsOfType<PersBulletController>();

        foreach (var persBulletController in persBullets) {
            Destroy(persBulletController.gameObject);
        }

        foreach (var enemyBulletController in enemyBullets) {
            Destroy(enemyBulletController.gameObject);
        }
    }

    private void StopTimerListener() {
        game = false;
    }

}
