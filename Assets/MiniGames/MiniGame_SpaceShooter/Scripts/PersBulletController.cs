﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PersBulletController : MonoBehaviour {

    private float speed = 10f;
    private bool move;
    private Vector3 target;
    private void Start() {
        move = true;
        target = new Vector3(transform.localPosition.x, 6f, 0);
    }

    void Update () {
		if (move && SpaceShooter_MainManager.instance.Game && !SpaceShooter_MainManager.instance.Pause) {
	        transform.localPosition = Vector3.MoveTowards(transform.localPosition, target, speed * Time.deltaTime);
	        if (transform.localPosition == target) {
	            Destroy(this.gameObject);
	        }
	    }
	}

    private void OnTriggerEnter() {
        Destroy(this.gameObject);
    }
}
